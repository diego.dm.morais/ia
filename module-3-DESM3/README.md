# Comandos


## create project
```shell
python3 -m venv ./.venv
```

```shell
source .venv/bin/activate
```


## install flask
```shell
pip install flask
```

## install as dependency
```shell
pip freeze > requeriments.txt
```

```shell
pip install -r ./requeriments.txt
```

## executar o app
```shell
python app.py
```
ou 
```shell
flask run 
```
## iniciar o banco de dados
```shell
flask db init
flask db migrate -m "primeira migração."
flask db upgrade
```

