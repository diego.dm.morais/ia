
from flask import Flask, render_template, request, jsonify
from flask_migrate import Migrate
from config import DevelopmentConfig
from models.user import User, db


app = Flask(__name__)
app.config.from_object(DevelopmentConfig())
db.init_app(app)
migrate = Migrate(app, db)


@app.route("/")
def index():
    return "<p>Olá</p>"


@app.route("/user/<name>")
def user(name):
    return render_template("user.html", name=name)


@app.route("/user", methods=["GET"])
def getAll():
    session = db.session()
    users = session.query(User).all()
    users_json = [user.serialize() for user in users]
    session.close()
    return jsonify(users_json), 200

@app.route("/user/<int:user_id>", methods=["GET"])
def by_id(user_id):
    session = db.session()
    user = session.query(User).get(user_id)
    session.close()
    if user:
      return jsonify(user.serialize()), 200
    return '', 404

@app.route('/user', methods=['POST'])
def create():
  body = request.get_json()
  session = db.session()
  try:
    user = User(nome=body['nome'],telefone=body['telefone'],email=body['email'])
    session.add(user)
    session.commit()
    return jsonify(user.serialize()), 200
  except Exception as e:
    print(e)
    session.rollback()
    return {"erro":"não conseguimos gravar o usuário"},404
  finally:
    session.close()

@app.route('/user/<int:user_id>', methods=['PATCH'])
def update(user_id):
  body = request.get_json()
  session = db.session()
  try:
    user:User = session.query(User).get(user_id)
    if "nome" in body:
       user.nome = body['nome']
    if "telefone" in body:
       user.telefone = body['telefone']
    if "email" in body:
       user.telefone = body['email']
    if "endereco" in body:
       user.telefone = body['endereco']
    if "cpf" in body:
       user.telefone = body['cpf']

    session.commit()
    return '', 204 
  except Exception as e:
    print(e)
    session.rollback()
    return {"erro":"não conseguimos gravar o usuário"}, 404
  finally:
    session.close()


@app.route('/user/<int:user_id>', methods=['DELETE'])
def delete(user_id):
  session = db.session()
  try:
    user:User = session.query(User).get(user_id)
    session.delete(user)
    session.commit()
    return '', 204 
  except Exception as e:
    print(e)
    session.rollback()
    return {"erro":"não conseguimos gravar o usuário"}, 404
  finally:
    session.close()


if __name__ == "__main__":
    app.run(debug=DevelopmentConfig.DEBUG, port=DevelopmentConfig.PORT_HOST)
