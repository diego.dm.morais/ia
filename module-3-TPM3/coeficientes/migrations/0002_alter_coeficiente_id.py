# Generated by Django 5.0.4 on 2024-04-04 14:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('coeficientes', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='coeficiente',
            name='id',
            field=models.AutoField(primary_key=True, serialize=False),
        ),
    ]
