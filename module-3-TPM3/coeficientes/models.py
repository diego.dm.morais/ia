from django.db import models

class Coeficiente(models.Model):
    id = models.AutoField(primary_key=True)
    number_a = models.FloatField('numero_a')
    number_b = models.FloatField('numero_b')
    number_c = models.FloatField('numero_c')
    result = models.CharField('result', max_length=250)
    
    def __str__(self):
        return f'ID: {self.id}, Numero A: {self.number_a}, Numero B: {self.number_b}, Numero C: {self.number_c}, Result: {self.result}'
