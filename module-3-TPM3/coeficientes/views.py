import math
from django.shortcuts import render

from coeficientes.models import Coeficiente


def index(request):
    if request.method == "GET":
        return render(request, "coeficientes/index.html")

    if request.method == "POST":
        a = float(request.POST.get("numero_a"))
        b = float(request.POST.get("numero_b"))
        c = float(request.POST.get("numero_c"))

        delta = b**2 - 4 * a * c
        delta = round(delta, 2)

        if delta > 0:
            x1 = (-b + math.sqrt(delta)) / (2 * a)
            x2 = (-b - math.sqrt(delta)) / (2 * a)
            x1 = round(x1, 2)
            x2 = round(x2, 2)
            result = f"Delta:{delta}, Raiz 1: {x1}, Raiz 2: {x2}"

        elif delta == 0:
            x = -b / (2 * a)
            x = round(x, 2)
            result = f"Delta:{delta}, Raiz: {x}"

        else:
            parte_real = -b / (2 * a)
            parte_imaginaria = math.sqrt(abs(delta)) / (2 * a)
            x1 = complex(parte_real, parte_imaginaria).real
            x2 = complex(parte_real, -parte_imaginaria).real
            x1 = round(x1, 2)
            x2 = round(x2, 2)
            result = f"Delta:{delta}, Raiz 1: {x1}, Raiz 2: {x2}"

        dao = Coeficiente(number_a=a, number_b=b, number_c=c, result=result)
        dao.save()

        context = {"resultado": result}

        return render(request, "coeficientes/index.html", context=context)


def results(request):
    if request.method == "GET":
        coeficientes = Coeficiente.objects.all()
        context = {"results": coeficientes}
        return render(request, "coeficientes/results.html", context=context)