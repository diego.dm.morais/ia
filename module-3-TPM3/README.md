# Comandos


## create project
```shell
python3 -m venv ./.venv
```

```shell
source .venv/bin/activate
```

```shell
pip install django
```

```shell
pip freeze > requeriments.txt
```

## install as dependency
```shell
pip install -r ./requeriments.txt
```

## create projeto django
```shell
django-admin startproject calculator
```

## Start project 
```shell
python manage.py runserver
```

## Create App Web
```shell
python manage.py startapp coeficientes
```

## Após criar o model executar os dois comando abaixo

```shell
python manage.py makemigrations
python manage.py migrate
python manage.py createsuperuser
```