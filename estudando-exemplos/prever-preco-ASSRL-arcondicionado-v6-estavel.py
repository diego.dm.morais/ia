import pandas as pd
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPRegressor
from sklearn.metrics import mean_squared_error

# Carregar dados do CSV
data = pd.read_csv("prever_preco_servico_v6.csv", sep=";")

# Substituir vírgulas por pontos e converter para float
data.replace(",", ".", regex=True, inplace=True)
data = data.apply(pd.to_numeric, errors="coerce")

# Remover linhas com valores ausentes
data.dropna(inplace=True)

# Separar os recursos (features) e rótulos (labels)
X = data.drop(columns=["preco_bruto", "preco_por_complexidade"])
y = data["preco_por_complexidade"]

# Dividir os dados em conjuntos de treinamento e teste
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=0)

# Padronizar os dados
scaler = StandardScaler()
X_train_scaled = scaler.fit_transform(X_train)
X_test_scaled = scaler.transform(X_test)

# Criar e treinar o modelo de rede neural
model = MLPRegressor(
    hidden_layer_sizes=(300, 150),
    activation="relu",
    solver="adam",
    max_iter=2000,
    learning_rate_init=0.001,
    random_state=50,
)
model.fit(X_train_scaled, y_train)

# Avaliar o desempenho do modelo
y_pred_train = model.predict(X_train_scaled)
y_pred_test = model.predict(X_test_scaled)

mse_train = mean_squared_error(y_train, y_pred_train)
mse_test = mean_squared_error(y_test, y_pred_test)

print("Erro médio quadrático (MSE) no conjunto de treinamento:", mse_train)
print("Erro médio quadrático (MSE) no conjunto de teste:", mse_test)

# Fazer previsões
# Substitua os valores abaixo pelos valores que você deseja prever
novo_dado = scaler.transform(
    [
        [
            1.00,
            1.00,
            0.00,
            0.00,
            0.00,
            0.00,
            12.50,
            0.00,
            30.00,
            30.00,
            0.00,
            0.00,
            0.90,
            1.20,
            0.30,
            0.00,
        ]
    ]
)
previsao = model.predict(novo_dado)
print("Previsão de preço:", previsao)
