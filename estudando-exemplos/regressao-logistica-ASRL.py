# Apredizado por regressão logistica
import pandas as pd
from sklearn.preprocessing import LabelEncoder, StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score

base = pd.read_csv("census.csv")

X = base.iloc[:, 0:-1].values
y = base.iloc[:, -1].values

# print(X[0])

def encode_categorical_features(X, categorical_indices):
    label_encoder = LabelEncoder()
    for col in categorical_indices:
        X[:, col] = label_encoder.fit_transform(X[:, col])
    return X

categorical_indices = [1, 3, 5, 6, 7, 8, 9, 13]
X = encode_categorical_features(X, categorical_indices)

# print(X[0])

scaler_x = StandardScaler()
X = scaler_x.fit_transform(X)

# print(X[0])

X_treinamento, X_teste, y_treinamento, y_test = train_test_split(X, y, test_size=0.4, random_state=0)

quantidade_registros = base.shape[0] # quantidade de registro: 32561
percentual_maximo_da_quantidade = 0.99 # percentual de registro: 50%
max_de_interacao = int(quantidade_registros*percentual_maximo_da_quantidade) # 50% dos quantidade de registro: 16280

classificador = LogisticRegression(max_iter=max_de_interacao)

classificador.fit(X=X_treinamento, y=y_treinamento)

previsao  = classificador.predict(X_teste)

print(previsao)
print(y_test)

taxa_de_acerto = accuracy_score(y_true=y_test, y_pred=previsao)

print(taxa_de_acerto)

print(classificador.predict(X_teste))


