# Apredizado por àrvode de desisão

import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.tree import DecisionTreeClassifier
from sklearn import tree
import matplotlib.pyplot as plt

# 1 - classificar os dados de entrada para os dados de saida.
df = pd.read_csv("risco_credito.csv")

# 2 - Seguimentando os dados
X = df.iloc[:, 0:-1].values
y = df.iloc[:, -1].values

# 3 - transformar em dados numericos os dados
# X[:,0] = ['Ruim', 'Desconhecida', 'Desconhecida', 'Desconhecida', 'Desconhecida', 'Desconhecida', 'Ruim', 'Ruim', 'Boa', 'Boa', 'Boa', 'Boa', 'Boa', 'Ruim']
# X[:,0] = [2 1 1 1 1 1 2 2 0 0 0 0 0 2]
label_enconder = LabelEncoder()
X[:,0] = label_enconder.fit_transform(X[:,0])
X[:,1] = label_enconder.fit_transform(X[:,1])
X[:,2] = label_enconder.fit_transform(X[:,2])
X[:,3] = label_enconder.fit_transform(X[:,3])


print(X[:,0])
arvore_risco_credito = DecisionTreeClassifier(criterion="entropy")
arvore_risco_credito.fit(X=X, y=y)

# retorno o percentual de importancia de cada coluna
# [historico  divida     garantias  renda]
# [0.41607015 0.10294068 0.03885431 0.44213486]
# print(arvore_risco_credito.feature_importances_)

# Busca as classes da arvore de risco
# ['Alto' 'Baixo' 'Moderado']
# print(arvore_risco_credito.classes_)

previsisores = ['historico', 'divida', 'garantias', 'renda']
figuras, eixo = plt.subplots(nrows=1, ncols=1, figsize=(10,10))

tree.plot_tree(arvore_risco_credito, feature_names=previsisores, class_names=arvore_risco_credito.classes_, filled=True)

previsao  = arvore_risco_credito.predict([[0,0,0,2]])
print(previsao)

# plt.show()

