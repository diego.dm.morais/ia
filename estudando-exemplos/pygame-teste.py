import pygame
from pygame.locals import *
import random
import time

# Inicialize o Pygame
pygame.init()

# Defina a largura e a altura da janela
WIDTH, HEIGHT = 800, 600
screen = pygame.display.set_mode((WIDTH, HEIGHT))

# Defina as cores
WHITE = (255, 255, 255)
RED = (255, 0, 0)

# Defina as coordenadas iniciais do quadrado
x, y = WIDTH // 2, HEIGHT // 2
square_size = 50

# Defina a velocidade de movimento inicial
velocidade = 0.3

# Defina uma lista para armazenar as bolas vermelhas
red_balls = []

# Contador de tempo para controlar a criação das bolas vermelhas
tempo_criacao_bolas = 0

# Contador de tempo total do jogo
tempo_total_jogo = 0

# Contador de bolas removidas
bolas_removidas = 0

# Fonte para exibir o número de bolas removidas
fonte = pygame.font.SysFont(None, 36)

# Função para criar uma bola vermelha aleatória em qualquer lugar da tela
def create_red_ball():
    x_pos = random.randint(0, WIDTH - 10)
    y_pos = random.randint(0, HEIGHT - 10)
    red_balls.append(pygame.Rect(x_pos, y_pos, 10, 10))

# Loop principal
running = True
while running:
    # Lidar com eventos
    for event in pygame.event.get():
        if event.type == QUIT:
            running = False

    # Capturar as teclas pressionadas
    keys = pygame.key.get_pressed()
    if keys[K_w] or keys[K_UP]:
        y -= velocidade
    if keys[K_s] or keys[K_DOWN]:
        y += velocidade
    if keys[K_a] or keys[K_LEFT]:
        x -= velocidade
    if keys[K_d] or keys[K_RIGHT]:
        x += velocidade

    # Limitar o movimento para dentro da tela
    if x < 0:
        x = 0
    elif x > WIDTH - square_size:
        x = WIDTH - square_size
    if y < 0:
        y = 0
    elif y > HEIGHT - square_size:
        y = HEIGHT - square_size

    # Verificar se o quadrado colidiu com alguma bola vermelha
    for ball in red_balls:
        if ball.colliderect((x, y, square_size, square_size)):
            red_balls.remove(ball)
            bolas_removidas += 1

    # Atualizar o tempo total do jogo
    tempo_total_jogo += 1

    # Aumentar a velocidade e o tempo de criação das bolas a cada 30 segundos
    if tempo_total_jogo % 1800 == 0:  # 30 segundos * 60 frames por segundo
        velocidade *= 1.02  # Aumenta a velocidade em 10%
        tempo_criacao_bolas *= 1.05  # Reduz o tempo de criação em 10%

    # Criar uma bola vermelha aleatória se for hora
    tempo_criacao_bolas += 1
    if tempo_criacao_bolas >= 1500:  # Defina a frequência inicial de criação de bolas
        create_red_ball()
        tempo_criacao_bolas = 0

    # Limpar a tela
    screen.fill(WHITE)

    # Desenhar o quadrado
    pygame.draw.rect(screen, (0, 0, 0), (x, y, square_size, square_size))

    # Desenhar as bolas vermelhas
    for ball in red_balls:
        pygame.draw.rect(screen, RED, ball)

    # Verificar se o tempo total do jogo atingiu 60 segundos
    if tempo_total_jogo >= 120000:  # 60 segundos * 60 frames por segundo
        # Exibir o número de bolas removidas
        mensagem_final = fonte.render(f"Fim de Jogo: {str(bolas_removidas)} pontos!" , True, (0, 0, 0))
        screen.blit(mensagem_final, (WIDTH // 2 - 150, HEIGHT // 2))
        pygame.display.update()
        time.sleep(5)  # Esperar 5 segundos antes de encerrar o jogo
        break

    # Atualizar a tela
    pygame.display.update()

# Finalizar o Pygame
pygame.quit()
