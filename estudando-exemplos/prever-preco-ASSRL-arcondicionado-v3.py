import locale
import warnings

import pandas as pd
from sklearn.linear_model import Ridge
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler

warnings.filterwarnings("ignore", "X does not have valid feature names")

locale.setlocale(locale.LC_ALL, "pt_BR.UTF-8")

# Carregar dados do CSV
data = pd.read_csv("prever_preco_servico_v4.csv", sep=";")

# Separar os dados em features (X) e target (y)
data.replace(",", ".", regex=True, inplace=True)
df = data.apply(pd.to_numeric, errors="coerce")
df.dropna(inplace=True)
X = df.iloc[:, :-1]
y = df.iloc[:, -1]

# Escalonamento de Recursos
scaler = StandardScaler()
X_scaled = scaler.fit_transform(X)

# Dividir os dados em conjunto de treinamento e teste
X_train, X_test, y_train, y_test = train_test_split(
    X_scaled, y, test_size=0.3, random_state=42
)

# Escolher e treinar o modelo de regressão Ridge com regularização
ridge_model = Ridge(alpha=0.1)  # Experimente diferentes valores de alpha
ridge_model.fit(X_train, y_train)

# Fazer previsões
predictions = ridge_model.predict(X_test)

# Avaliar o desempenho do modelo
mse = mean_squared_error(y_test, predictions)
print("Erro médio quadrático:", mse)

# Aplicar a mesma transformação aos dados de entrada
dados_input_scaled = scaler.transform(
    [
        [
            1,
            2,
            0,
            0,
            0,
            0,
            1,
            55.00,
            2.48,
            2.75,
            1.65,
            0,
            12.5,
            0
        ]
    ]
)

# Fazer previsões com os dados de entrada transformados
previsoes = ridge_model.predict(dados_input_scaled)

for previsao in previsoes:
    print("Preço estimado do serviço:", locale.currency(previsao, grouping=True))
