import pandas as pd
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPRegressor
from sklearn.preprocessing import StandardScaler, LabelEncoder
from sklearn.ensemble import RandomForestClassifier  # Importe RandomForestClassifier


df = pd.read_csv("census.csv")

print(df.shape)
# remover linha que está em branco
df.dropna(inplace=True)
X = df.iloc[:, :-1]
y = df.iloc[:, -1]

# Pré-processamento dos dados categóricos
label_encoder = LabelEncoder()
for column in X.select_dtypes(include=['object']).columns:
    X[column] = label_encoder.fit_transform(X[column])
y = label_encoder.fit_transform(y)

# Dividir os dados em conjuntos de treinamento e teste
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=0)

# Padronizar os dados
scaler = StandardScaler()
X_train_scaled = scaler.fit_transform(X_train)
X_test_scaled = scaler.transform(X_test)

# Criar e treinar o modelo de rede neural
model = MLPRegressor(
    hidden_layer_sizes=(100, 50),
    activation="relu",
    solver="adam",
    max_iter=9000,
    learning_rate_init=0.001,
    random_state=50,
)
model.fit(X_train_scaled, y_train)

# Prever com o modelo treinado
y_pred_train = model.predict(X_train_scaled)
y_pred_test = model.predict(X_test_scaled)

# Calcular o erro médio quadrático (MSE)
mse_train = mean_squared_error(y_train, y_pred_train)
mse_test = mean_squared_error(y_test, y_pred_test)

print("Erro médio quadrático (MSE) no conjunto de treinamento:", mse_train)
print("Erro médio quadrático (MSE) no conjunto de teste:", mse_test)

# Criar e treinar o modelo de RandomForestClassifier
random_forest_census = RandomForestClassifier(n_estimators=100, criterion='entropy', random_state=0)
random_forest_census.fit(X_train, y_train)  # Usando os dados não escalados para RandomForestClassifier

# Resultados do modelo RandomForestClassifier
train_score = random_forest_census.score(X_train, y_train)
test_score = random_forest_census.score(X_test, y_test)

print("Acurácia do conjunto de treinamento:", train_score)
print("Acurácia do conjunto de teste:", test_score)