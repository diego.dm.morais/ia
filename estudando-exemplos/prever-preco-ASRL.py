""" 
    ASRL(Apredizado supervisionado regressão linear)

    Esta inteligência artificial foi criada com o propósito de estimar os custos dos serviços de desenvolvimento de software, 
    fornecendo assim uma estimativa precisa do preço final.

    Soma total dos preços dos novos serviços: R$ 409,20

    O código fornecido realiza aprendizado supervisionado. Isso é indicado pelas seguintes características:
        Os dados de entrada (X) e de saída (y) são fornecidos e usados para treinar o modelo. 
        Os dados de entrada representam as características dos serviços de desenvolvimento de software 
        (como valor da hora, nível do desenvolvedor, complexidade do projeto, etc.), 
        enquanto os dados de saída representam os preços desses serviços. 
        Portanto, há uma supervisão clara entre os dados de entrada e de saída, 
        o que caracteriza o aprendizado supervisionado.
    
    O modelo é treinado com base em pares de entrada-saída conhecidos, 
    utilizando o algoritmo de regressão linear com regularização (Ridge). 
    Durante o treinamento, o modelo ajusta seus parâmetros para 
    minimizar a diferença entre os valores previstos e os valores reais dos 
    preços dos serviços de desenvolvimento de software.
    
    Após o treinamento, o modelo é avaliado com dados de teste, 
    e seu desempenho é medido usando uma métrica de pontuação (score).
    
    Finalmente, o modelo treinado é usado para fazer previsões 
    sobre o preço de novos serviços de desenvolvimento de software com base em suas características.
    
    Em resumo, o código implementa um modelo de regressão linear para prever o 
    preço de serviços de desenvolvimento de software com base em várias características, 
    o que caracteriza um caso típico de aprendizado supervisionado.
"""

import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import PolynomialFeatures
from sklearn.linear_model import Ridge
from sklearn.pipeline import make_pipeline
import locale

locale.setlocale(locale.LC_ALL, "pt_BR.UTF-8")

# Gerando dados de exemplo
np.random.seed(0)
valor_hora = np.random.randint(100.0, 350.0, 100000)
tipo_trabalho = np.random.randint(0, 1, 100000)
nivel_desenvolvedor = np.random.randint(1, 3, 100000)
complex_project = np.random.randint(1, 3, 100000)
tempo = np.random.randint(1, 10, 100000)

VARIACAO = 1
# VARIACAO = 130

preco = (
    valor_hora * VARIACAO
    + nivel_desenvolvedor * VARIACAO
    + complex_project * VARIACAO
    + tempo * VARIACAO
    + tipo_trabalho * VARIACAO
)
X = np.column_stack((valor_hora, nivel_desenvolvedor, complex_project, tempo, tipo_trabalho))
y = preco

# Dividindo os dados em conjuntos de treinamento e teste
X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.36, random_state=0
)

# Criando e treinando o modelo
modelo = make_pipeline(PolynomialFeatures(degree=2), Ridge(alpha=0.001))
modelo.fit(X_train, y_train)

# Fazendo previsões para o conjunto de teste
previsoes = modelo.predict(X_test)

# Avaliando o modelo
score = modelo.score(X_test, y_test)
print("Score do modelo:", score)

def calcular_preco_servico(valores):
    novo_servico = np.array([valores])
    preco_novo_servico = modelo.predict(novo_servico)
    print("Preço do serviço:", locale.currency(preco_novo_servico[0], grouping=True))
    return preco_novo_servico[0]

# Lista de novos serviços
# valor_hora, nivel_desenvolvedor, complex_project, tempo, tipo_trabalho
novos_servicos = [
    [220,   3, 3, 10, 1],   # Desenvolvedor front-end pleno em Next.js
    [40,    1, 1, 1, 1],    # Desenvolvedor back-end pleno em Python
    [40,    1, 1, 1, 1],   # Desenvolvedor fullstack senior em Python, Next.js e PostgreSQL
    [40,    1, 1, 1, 1],     # Engenheiro de Software Senior
    [40,    1, 1, 1, 1]      # Tech Lead Senior
]

# Somando todos os preços dos novos serviços
soma_total = sum([calcular_preco_servico(servico) for servico in novos_servicos])
print("Soma total dos preços dos novos serviços:", locale.currency(soma_total, grouping=True))