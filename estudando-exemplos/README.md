## create project
```shell
python3 -m venv ./.venv
```

```shell
source .venv/bin/activate
```

## install as dependency
```shell
pip install -r ./requeriments.txt
```

## anotation 

Claro, vou classificar cada tipo de aprendizado e fornecer dois cenários para cada um, juntamente com seus objetivos:

1. **Aprendizado não-supervisionado - Regressão Linear:**

   - **Cenário 1:** Análise de mercado
     - Objetivo: Em um conjunto de dados com informações sobre diferentes produtos e seus preços, usar regressão linear não supervisionada para identificar padrões nos preços dos produtos e segmentar o mercado com base nesses padrões.

   - **Cenário 2:** Análise de desempenho de funcionários
     - Objetivo: Analisar dados de desempenho de funcionários em uma empresa e usar regressão linear não supervisionada para identificar possíveis correlações entre variáveis como horas de trabalho, experiência e desempenho, sem depender de rótulos predefinidos.

   - **Cenário 3:** Análise de dados de sensor de IoT
     - Objetivo: Usar regressão linear não supervisionada para analisar dados coletados de sensores em dispositivos IoT (Internet das Coisas) e identificar padrões ou anomalias que possam indicar falhas nos dispositivos ou otimizações no uso.

   - **Cenário 4:** Segmentação de clientes em um site de e-commerce
     - Objetivo: Utilizar dados de navegação e histórico de compras de clientes para segmentá-los em grupos semelhantes com base em seus padrões de compra, empregando regressão linear não supervisionada para personalizar recomendações e estratégias de marketing.

2. **Aprendizado supervisionado - Regressão Linear:**

    - **Cenário 1:** Previsão de vendas
        - Objetivo: Utilizar dados históricos de vendas de uma loja para prever as vendas futuras, empregando uma abordagem de regressão linear supervisionada, onde as características incluem fatores como preço, promoções e temporada.

    - **Cenário 2:** Previsão de preço de imóveis
        - Objetivo: Prever o preço de venda de imóveis com base em características como área, número de quartos, localização, etc., utilizando um modelo de regressão linear supervisionada treinado com dados históricos de vendas.
   
    - **Cenário 3:** Previsão de demanda de energia elétrica
        - Objetivo: Utilizar dados históricos de consumo de energia elétrica juntamente com fatores como temperatura, dia da semana e feriados para prever a demanda futura de energia, aplicando regressão linear supervisionada para otimizar o planejamento de recursos energéticos.

    - **Cenário 4:** Previsão de preço de ações
        - Objetivo: Desenvolver um modelo de regressão linear supervisionada para prever os preços futuros das ações com base em dados históricos de preços, volumes de negociação e indicadores de mercado, auxiliando investidores na tomada de decisões.


3. **Aprendizado - Árvore de Decisão:**

    - **Cenário 1:** Classificação de clientes de um banco
        - Objetivo: Usar uma árvore de decisão para classificar os clientes de um banco em categorias de risco de crédito, com base em características como histórico de crédito, renda, idade, etc., para auxiliar na tomada de decisões de concessão de empréstimos.

    - **Cenário 2:** Diagnóstico médico
        - Objetivo: Desenvolver um sistema de suporte à decisão médica que utilize uma árvore de decisão para classificar pacientes com base em sintomas, histórico médico e resultados de testes, visando auxiliar os médicos no diagnóstico de doenças.
    
    - **Cenário 3:** Diagnóstico de problemas de rede
        - Objetivo: Utilizar uma árvore de decisão para diagnosticar problemas de rede com base em informações como tipo de falha, localização física, horário de ocorrência e impacto nos serviços, agilizando o processo de resolução de problemas.

    - **Cenário 4:** Classificação de espécies de plantas
        - Objetivo: Desenvolver um sistema automatizado que utilize uma árvore de decisão para classificar diferentes espécies de plantas com base em características como altura, forma das folhas e padrão de crescimento, beneficiando a botânica e a agricultura.


4. **Aprendizado - Regressão Logística:**

    - **Cenário 1:** Previsão de probabilidade de falha de equipamentos
        - Objetivo: Utilizar regressão logística para prever a probabilidade de falha de equipamentos industriais com base em variáveis como tempo de operação, temperatura, pressão, etc., permitindo a manutenção preventiva.

    - **Cenário 2:** Detecção de spam em e-mails
        - Objetivo: Desenvolver um modelo de regressão logística para classificar e-mails como spam ou não-spam com base em características como palavras-chave, remetente, presença de anexos, etc., para melhorar a eficácia dos filtros de spam.

    - **Cenário 3:** Previsão de taxa de conversão em marketing digital
        - Objetivo: Utilizar regressão logística para prever a taxa de conversão de campanhas de marketing digital com base em métricas como cliques, impressões, segmentação de público-alvo, etc., auxiliando na alocação eficiente de recursos de marketing.

    - **Cenário 4:** Detecção de fraudes em transações financeiras
        - Objetivo: Desenvolver um sistema de detecção de fraudes que utilize regressão logística para analisar padrões de transações, como valor, localização e histórico do cliente, e identificar transações suspeitas que requerem investigação adicional.
Esses cenários ilustram diferentes aplicações dos tipos de aprendizado mencionados, demonstrando seus usos em problemas do mundo real e seus respectivos objetivos.
