""" 
    ANSRL(Apredizado semi-supervisionado regressão linear)
    
    Este código tem como objetivo coletar informações para precificar o valor de um serviço de instalação de ar condicionado.

    Instalção simples de 1 arcodicionado em casa: R$ 400,00
    Instalção simples de 1 arcodicionado no APTO: R$ 500,00

    Quanto esta sua hora: 44.00
    Quanto de hora vai gasta: 4
    Complexidade do trabalho = 1 SIMPLES, 2 MEDIO , 3 COMPLEXO
    Custo do dia: 105,0
    Custo da ferramenta: 30,00
    Custo do materia sem comissão: 250,00
    Com ou sem ajudante: 0 sem ajudante e 1 com ajudante

"""

import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import PolynomialFeatures
from sklearn.linear_model import Ridge
from sklearn.pipeline import make_pipeline
from sklearn.cluster import KMeans
import locale

locale.setlocale(locale.LC_ALL, "pt_BR.UTF-8")

np.random.seed(0)

PRECO_DO_COMBUSTIVEL = 3.94
PERCENTUAL_NA_FERRAMENTAS = 0.12
PERCENTUAL_CUSTO_MANUTENCAO_VEICULO = 0.2
PERCENTUAL_IMPOSTO_POR_HORA = 0.3
K_L_O_CARRO_FAZ = 9
BALANCO_COMPLEXDADE = 10
VALOR_HORA = 46.00
VALOR_HORA_AJUDATANTE = 12.5
CUSTO_HORA_VEICULO_MANUTENCAO = VALOR_HORA * PERCENTUAL_CUSTO_MANUTENCAO_VEICULO
CUSTO_HORA_FERRAMENTAS = PERCENTUAL_NA_FERRAMENTAS * VALOR_HORA
CUSO_HORA_IMPOSTO = PERCENTUAL_IMPOSTO_POR_HORA * VALOR_HORA
CUSTO_DISTANCIA_VEICULO = PRECO_DO_COMBUSTIVEL / K_L_O_CARRO_FAZ
MARGEM_DO_PRODUTO = 0.1
BALANCO_VARIADO = 1.2
PERCENTUAL_DE_DESCONTO = 0.00  # 5.28% DE DESCONTO
SIZE = 1000

tempo_instalacao = np.full(SIZE, 3)
complexidade_instalacao = np.random.randint(1, 4, size=SIZE)
custo_pessoal = np.random.randint(50, 150, size=SIZE)
custo_kit_instalacao = np.random.choice([227.00, 267.00, 347.00], size=SIZE)
com_e_sem_ajuda = np.random.choice([0, 2], size=SIZE)
kilomentragem = np.random.randint(1, 15, size=SIZE)
tempo_do_ajudante = np.random.randint(1, 16, size=SIZE)

custo = (
    VALOR_HORA
    + PERCENTUAL_IMPOSTO_POR_HORA
    + CUSTO_HORA_FERRAMENTAS
    + CUSTO_HORA_VEICULO_MANUTENCAO
    + CUSO_HORA_IMPOSTO
)
preco = (
    +tempo_instalacao * custo * BALANCO_VARIADO
    + complexidade_instalacao * BALANCO_COMPLEXDADE * BALANCO_VARIADO
    + custo_pessoal * BALANCO_VARIADO
    + custo_kit_instalacao * MARGEM_DO_PRODUTO * BALANCO_VARIADO
    + com_e_sem_ajuda * BALANCO_VARIADO
    + kilomentragem * CUSTO_DISTANCIA_VEICULO * BALANCO_VARIADO
    + tempo_do_ajudante * (VALOR_HORA_AJUDATANTE + PERCENTUAL_IMPOSTO_POR_HORA) * BALANCO_VARIADO
)
X = np.column_stack(
    (
        tempo_instalacao,
        complexidade_instalacao,
        custo_pessoal,
        custo_kit_instalacao,
        com_e_sem_ajuda,
        kilomentragem,
        tempo_do_ajudante,
    )
)
y = preco - preco * PERCENTUAL_DE_DESCONTO

# Dividindo os dados em conjunto rotulado e não rotulado
X_labeled, X_unlabeled, y_labeled, y_unlabeled = train_test_split(
    X, y, test_size=0.5, random_state=0
)

# Treinando o modelo inicial com dados rotulados
modelo = make_pipeline(PolynomialFeatures(degree=2), Ridge(alpha=0.001))
modelo.fit(X_labeled, y_labeled)

# Aplicando o algoritmo K-means aos dados não rotulados
kmeans = KMeans(n_clusters=5, random_state=0)
clusters = kmeans.fit_predict(X_unlabeled)

# Calculando a média de preço para cada cluster
media_precos = []
for cluster_id in range(5):
    preco_cluster = y_unlabeled[clusters == cluster_id]
    media_precos.append(np.mean(preco_cluster))

# Estimando o preço dos dados não rotulados com base nos clusters
precos_estimados = []
for cluster_id in kmeans.predict(X_unlabeled):
    preco_estimado = media_precos[cluster_id]
    precos_estimados.append(preco_estimado)

# Avaliando o desempenho do modelo com dados rotulados e não rotulados
score_labeled = modelo.score(X_labeled, y_labeled)
score_unlabeled = modelo.score(X_unlabeled, precos_estimados)
print("Score do modelo com dados rotulados:", score_labeled)
print("Score do modelo com dados não rotulados:", score_unlabeled)


# Função para estimar o preço de um novo serviço
def estimar_preco_servico(valores):
    preco_estimado = modelo.predict([valores])[0]
    print(
        "Preço por estimativa:",
        locale.currency(preco_estimado, grouping=True),
    )
    return preco_estimado


# Lista de novos serviços
#   tempo_serviço,
#   complexidade_serviço,
#   custo_alimentacao,
#   custo_material,
#   funcionario, 0=não 1=sim
#   kilometragem,
#   tempo em horas do ajudante
novos_servicos = [
    [4, 0, 25, 250, 0, 10, 0],
]


soma_total = sum([estimar_preco_servico(servico) for servico in novos_servicos])
print(
    "Preço total do serviço:",
    locale.currency(soma_total, grouping=True),
)


# FIXME mudar para # 3 - transformar em dados classificação numericas usando from sklearn.preprocessing import LabelEncoder