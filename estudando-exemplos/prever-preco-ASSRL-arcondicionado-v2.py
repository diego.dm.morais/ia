import locale
import warnings

import pandas as pd
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.model_selection import cross_val_score, train_test_split
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import PolynomialFeatures, StandardScaler

warnings.filterwarnings("ignore", "X does not have valid feature names")

locale.setlocale(locale.LC_ALL, "pt_BR.UTF-8")

# Carregar os dados
df = pd.read_csv("prever_preco_servico-v4.csv", sep=";")
df.drop(
    columns=[
        "preco_bruto",
        "valor_manutentacao_veiculo",
        "valor_imposto",
        "valor_ferramentas",
        "valor_km",
        "margem_material",
        "valor_hora_funcionario",
    ],
    inplace=True,
)
df.replace(",", ".", regex=True, inplace=True)
df = df.apply(pd.to_numeric, errors="coerce")
df.dropna(inplace=True)
X = df.iloc[:, :-1]
y = df.iloc[:, -1]

# Dividir os dados em treinamento e teste
X_treinamento, X_teste, y_treinamento, y_teste = train_test_split(
    X, y, test_size=0.1, random_state=45
)

# Definir os pesos para cada parâmetro de entrada
pesos = [1, 1, 0, 0, 0, 0, 0, 1]  # Exemplo de pesos

# Multiplicar os dados de entrada pelos pesos correspondentes
X_treinamento_pesado = X_treinamento * pesos
X_teste_pesado = X_teste * pesos

# Criar pipeline
pipeline = Pipeline(
    [
        ("scaler", StandardScaler()),
        ("polynomial_features", PolynomialFeatures(degree=1)),
        ("regressor", LinearRegression()),
    ]
)
# Treinar o modelo usando validação cruzada
scores = cross_val_score(pipeline, X_treinamento, y_treinamento, cv=5)

print("Média dos scores de validação cruzada:", scores.mean())

# Treinar o modelo final
pipeline.fit(X_treinamento_pesado, y_treinamento)

# Avaliar o modelo nos dados de teste
y_pred = pipeline.predict(X_teste)
mse = mean_squared_error(y_teste, y_pred)
r2 = r2_score(y_teste, y_pred)

print("Erro quadrático médio:", mse)
print("Coeficiente de determinação (R²):", r2)

# Prever preço do serviço com novos dados de entrada

# tempo_estimado_h
# complexidade
# custo_pessoal
# custo_material_embutido
# custo_material_compra
# km_ate_o_cliente
# tempo_h_ajudante
# valor_hora

dados_input = [[8, 2, 25, 0, 0, 0, 0, 55]]  # Lista de listas

previsoes = pipeline.predict(dados_input)

for previsao in previsoes:
    print("Preço estimado do serviço:", locale.currency(previsao, grouping=True))
