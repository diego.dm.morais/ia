""" 
    ANSRL(Apredizado semi-supervisionado regressão linear)
    
    Esta inteligência artificial foi criada com o propósito de estimar os custos dos serviços de desenvolvimento de software, 
    fornecendo assim uma estimativa precisa do preço final.

    Soma total das estimativas de preço dos novos serviços: R$ 414,20
"""


import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import PolynomialFeatures
from sklearn.linear_model import Ridge
from sklearn.pipeline import make_pipeline
from sklearn.cluster import KMeans
import locale

locale.setlocale(locale.LC_ALL, "pt_BR.UTF-8")

# Gerando dados de exemplo
np.random.seed(0)
valor_hora = np.random.randint(100.0, 350.0, 100000)
tipo_trabalho = np.random.randint(0, 2, 100000)
nivel_desenvolvedor = np.random.randint(1, 3, 100000)
complex_project = np.random.randint(1, 3, 100000)
tempo = np.random.randint(1, 10, 100000)

# VARIACAO = 130
VARIACAO = 1

preco = (
    valor_hora * VARIACAO
    + nivel_desenvolvedor * VARIACAO
    + complex_project * VARIACAO
    + tempo * VARIACAO
    + tipo_trabalho * VARIACAO
)
X = np.column_stack((valor_hora, nivel_desenvolvedor, complex_project, tempo, tipo_trabalho))
y = preco

# Dividindo os dados em conjunto rotulado e não rotulado
X_labeled, X_unlabeled, y_labeled, y_unlabeled = train_test_split(
    X, y, test_size=0.9, random_state=0
)

# Treinando o modelo inicial com dados rotulados
modelo = make_pipeline(PolynomialFeatures(degree=2), Ridge(alpha=0.001))
modelo.fit(X_labeled, y_labeled)

# Aplicando o algoritmo K-means aos dados não rotulados
kmeans = KMeans(n_clusters=5, random_state=0)
clusters = kmeans.fit_predict(X_unlabeled)

# Calculando a média de preço para cada cluster
media_precos = []
for cluster_id in range(5):
    preco_cluster = y_unlabeled[clusters == cluster_id]
    media_precos.append(np.mean(preco_cluster))

# Estimando o preço dos dados não rotulados com base nos clusters
precos_estimados = []
for cluster_id in kmeans.predict(X_unlabeled):
    preco_estimado = media_precos[cluster_id]
    precos_estimados.append(preco_estimado)

# Avaliando o desempenho do modelo com dados rotulados e não rotulados
score_labeled = modelo.score(X_labeled, y_labeled)
score_unlabeled = modelo.score(X_unlabeled, precos_estimados)
print("Score do modelo com dados rotulados:", score_labeled)
print("Score do modelo com dados não rotulados:", score_unlabeled)

# Função para estimar o preço de um novo serviço
def estimar_preco_servico(valores):
    preco_estimado = modelo.predict([valores])[0]
    print("Estimativa de preço do serviço:", locale.currency(preco_estimado, grouping=True))
    return preco_estimado

# Lista de novos serviços
# valor_hora, nivel_desenvolvedor, complex_project, tempo, tipo_trabalho
novos_servicos = [
    [220,   3, 3, 10, 1],   # Desenvolvedor front-end pleno em Next.js
    [40,    1, 1, 1, 1],    # Desenvolvedor back-end pleno em Python
    [40,    1, 1, 1, 1],   # Desenvolvedor fullstack senior em Python, Next.js e PostgreSQL
    [40,    1, 1, 1, 1],     # Engenheiro de Software Senior
    [40,    1, 1, 1, 1]      # Tech Lead Senior
]

# Estimando o preço de cada novo serviço
soma_total = sum([estimar_preco_servico(servico) for servico in novos_servicos])
print("Soma total das estimativas de preço dos novos serviços:", locale.currency(soma_total, grouping=True))
