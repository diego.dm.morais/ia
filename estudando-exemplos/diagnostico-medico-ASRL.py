import numpy as np
import pandas as pd
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, classification_report, confusion_matrix

# Carregando o conjunto de dados fictício

data = {
    'Idade': [10,30,60,80],
    'PressaoArterial': [90,120,160,100],
    'Colesterol': [200,250,300,200],
    'Fumante': [0, 1, 1, 0],
    'Diagnostico': [0, 1, 1, 0]  # 0 - Não tem a doença, 1 - Tem a doença
}

df = pd.DataFrame(data)

# Convertendo variáveis categóricas em numéricas
df['Diagnostico'] = df['Diagnostico'].map({0: 'não', 1: 'sim'})

# Separando features (X) e target (y)
X = df[['Idade', 'PressaoArterial', 'Colesterol', 'Fumante']]
y = df['Diagnostico']

# Dividindo os dados em conjuntos de treinamento e teste
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.4, random_state=0)

# Padronizando os recursos (opcional, mas recomendado para regressão logística)
scaler = StandardScaler()
X_train_scaled = scaler.fit_transform(X_train)
X_test_scaled = scaler.transform(X_test)

# Inicializando e treinando o modelo de regressão logística
model = LogisticRegression()
model.fit(X_train_scaled, y_train)

# Fazendo previsões
y_pred = model.predict(X_test_scaled)

# Avaliando o desempenho do modelo
accuracy = accuracy_score(y_test, y_pred)
print("Acurácia do modelo:", accuracy)


# Matriz de confusão
print("\nMatriz de Confusão:")
conf_matrix = confusion_matrix(y_test, y_pred)
print(conf_matrix)


# Gerando novos dados fictícios
new_data = {
    'Idade': [25],
    'PressaoArterial': [100],
    'Colesterol': [200],
    'Fumante': [0]
}

new_df = pd.DataFrame(new_data)

# Padronizando os novos dados (da mesma forma que fizemos com os dados de treinamento)
new_data_scaled = scaler.transform(new_df)

# Fazendo previsões com os novos dados
predictions = model.predict(new_data_scaled)

# Imprimindo as previsões
print("Previsões para os novos dados:")
print(predictions)