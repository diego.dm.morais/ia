from sklearn.datasets import fetch_openml
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPClassifier
import pandas as pd
import keras

import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense



boston = fetch_openml(name="boston", version=1)

X = pd.DataFrame(data=boston.data, columns=boston.feature_names)
y = boston.target

X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.2, random_state=42
)

model = Sequential()
model.add(keras.layers.Dense(units=5, activation="relu", input_shape=X_train.shape[1:]))

model.compile(optimizer='adam',
              loss='binary_crossentropy',  # Para um problema de classificação binária
              metrics=['accuracy'])

model.summary()