import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.neural_network import MLPRegressor
from sklearn.metrics import mean_squared_error

start_date = pd.Timestamp("2023-01-01")
end_date = pd.Timestamp("2024-01-01")
price_range = (61000.00, 73000.00)
volume_range = (24988.45, 1758820969.32)

# Gerar dados aleatórios
num_days = (end_date - start_date).days + 1  # Adicionando 1 para incluir a data final
data = {
    "time": pd.date_range(start=start_date, end=end_date).astype(np.int64) // 10**9,
    "high": np.random.uniform(*price_range, size=num_days),
    "low": np.random.uniform(*price_range, size=num_days),
    "open": np.random.uniform(*price_range, size=num_days),
    "volumefrom": np.random.uniform(*volume_range, size=num_days),
    "volumeto": np.random.uniform(*volume_range, size=num_days),
    "close": np.random.uniform(*price_range, size=num_days)
}

# Converter para DataFrame
df = pd.DataFrame(data)

# Separar features (X) e target (y)
X = df.drop(['close'], axis=1)
y = df['close']

# Dividir os dados em conjuntos de treinamento e teste
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Normalizar os dados
scaler = StandardScaler()
X_train_scaled = scaler.fit_transform(X_train)
X_test_scaled = scaler.transform(X_test)

# Treinar o modelo de rede neural com hiperparâmetros ajustados
model = MLPRegressor(hidden_layer_sizes=(100, 50), alpha=0.001, learning_rate_init=0.01, max_iter=1000, random_state=42)
model.fit(X_train_scaled, y_train)

# Fazer previsões
y_pred = model.predict(X_test_scaled)

# Avaliar o desempenho do modelo
mse = mean_squared_error(y_test, y_pred)
print("Erro quadrático médio:", mse)

# Prever o preço do Bitcoin para o próximo dia
# Aqui está um exemplo fictício de dados para o próximo dia
next_day_data = {"time": pd.Timestamp("2024-01-02").timestamp(), "high": 66000, "low": 64000, "open": 65000, "volumefrom": 2000, "volumeto": 150000000, "close": 0}
next_day_X = pd.DataFrame([next_day_data])

# Criar um novo scaler excluindo a coluna 'close'
scaler_next_day = StandardScaler()
X_next_day_scaled = scaler_next_day.fit_transform(next_day_X.drop(['close'], axis=1))

# Adicionar a previsão do modelo para a coluna 'close' dos dados do próximo dia
next_day_X_scaled = pd.DataFrame(X_next_day_scaled, columns=next_day_X.drop(['close'], axis=1).columns)
next_day_X_scaled['close'] = model.predict(X_next_day_scaled)

print("Preço previsto do Bitcoin para o próximo dia:", next_day_X_scaled['close'].values[0])
