import tensorflow as tf
from tensorflow.keras.datasets import mnist
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Flatten

# Carregar dados MNIST e dividir em conjuntos de treinamento e teste
(train_images, train_labels), (test_images, test_labels) = mnist.load_data()

# Pré-processamento dos dados
train_images = train_images / 255.0
test_images = test_images / 255.0

# Construir o modelo MLP
model = Sequential([
    Flatten(input_shape=(28, 28)),  # Camada de entrada para converter imagem 2D em vetor 1D
    Dense(128, activation='relu'),  # Camada oculta com 128 neurônios e função de ativação ReLU
    Dense(10, activation='softmax')  # Camada de saída com 10 neurônios para classificação dos dígitos (0-9)
])

# Compilar o modelo
model.compile(optimizer='adam',
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])

# Treinar o modelo
model.fit(train_images, train_labels, epochs=5)

# Avaliar o modelo
test_loss, test_acc = model.evaluate(test_images, test_labels)
print('Acurácia do teste:', test_acc)
