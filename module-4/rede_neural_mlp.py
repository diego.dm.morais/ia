import pandas as pd
from sklearn.datasets import load_breast_cancer
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import accuracy_score, classification_report

# Carregar o conjunto de dados de câncer de mama do Wisconsin
data = load_breast_cancer()
X = pd.DataFrame(data.data, columns=data.feature_names)
y = data.target

# Dividir os dados em conjunto de treinamento e teste
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Normalizar os dados
scaler = StandardScaler()
X_train_scaled = scaler.fit_transform(X_train)
X_test_scaled = scaler.transform(X_test)

# Inicializar e treinar o classificador MLP
mlp_classifier = MLPClassifier(hidden_layer_sizes=(100,), max_iter=1000, random_state=42)
mlp_classifier.fit(X_train_scaled, y_train)

# Prever os rótulos no conjunto de teste
y_pred = mlp_classifier.predict(X_test_scaled)

# Avaliar o desempenho do classificador
accuracy = accuracy_score(y_test, y_pred)
print("Acurácia do classificador MLP:", accuracy)

# Relatório de classificação
print("\nRelatório de classificação:")
print(classification_report(y_test, y_pred))


# Adicionar um novo dado (um exemplo)
new_data = [[13.0, 21.82, 87.5, 519.8, 0.1273, 0.1932, 0.1859, 0.0935, 0.2350, 0.0739, 0.3063, 1.002, 2.406, 24.32, 0.006608, 0.03125, 0.03479, 0.01452, 0.01996, 0.00444, 14.0, 29.0, 88.0, 600.0, 0.15, 0.2836, 0.2487, 0.09783, 0.2996, 0.08405]]

# Normalizar o novo dado usando o mesmo scaler
new_data_scaled = scaler.transform(new_data)

# Prever se o novo dado representa um tumor maligno ou benigno
prediction = mlp_classifier.predict(new_data_scaled)
print("\nPrevisão para o novo dado:", "Maligno" if prediction[0] == 1 else "Benigno")
