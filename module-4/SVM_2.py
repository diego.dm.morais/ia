import numpy as np
import plotly.express as px
import plotly.graph_objects as go
from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC
from sklearn.metrics import accuracy_score, classification_report

# Carregar o conjunto de dados de exemplo (será substituído por seus dados reais)
# Aqui estamos usando o conjunto de dados iris apenas para fins de demonstração
# Você precisará substituir isso pelo seu conjunto de dados de imagens de ressonância magnética
iris = datasets.load_iris()
X = iris.data[:, :2]  # Usamos apenas as duas primeiras características para simplificar
y = iris.target

# Dividir o conjunto de dados em conjunto de treinamento e conjunto de teste
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=42)

# Inicializar e treinar o modelo SVM
svm = SVC(kernel='rbf', C=1.0, random_state=42)
svm.fit(X_train, y_train)

# Fazer previsões no conjunto de teste
y_pred = svm.predict(X_test)

# Calcular a acurácia
accuracy = accuracy_score(y_test, y_pred)
print("Acurácia:", accuracy)

# Imprimir o relatório de classificação
print("Relatório de Classificação:")
print(classification_report(y_test, y_pred))

# Visualizar as fronteiras de decisão
# Esta parte só funcionará se você tiver apenas 2 características
# Substitua X_train[:, 0] e X_train[:, 1] pelas suas características reais

# Gerar uma grade de pontos para plotar as fronteiras de decisão
x_min, x_max = X_train[:, 0].min() - 1, X_train[:, 0].max() + 1
y_min, y_max = X_train[:, 1].min() - 1, X_train[:, 1].max() + 1
xx, yy = np.meshgrid(np.arange(x_min, x_max, 0.02),
                     np.arange(y_min, y_max, 0.02))
Z = svm.predict(np.c_[xx.ravel(), yy.ravel()])
Z = Z.reshape(xx.shape)

# Plotar os pontos de treinamento
fig = px.scatter(x=X_train[:, 0], y=X_train[:, 1], color=y_train)
fig.update_traces(marker=dict(size=12))
fig.update_layout(title="Conjunto de Treinamento", xaxis_title="Feature 1", yaxis_title="Feature 2")

# Adicionar as fronteiras de decisão
fig.add_trace(go.Contour(x=np.arange(x_min, x_max, 0.02),
                          y=np.arange(y_min, y_max, 0.02),
                          z=Z,
                          colorscale='Viridis',
                          showscale=False,
                          opacity=0.5,
                          name='Fronteira de Decisão'))

fig.show()
