import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.svm import SVC
from sklearn.preprocessing import StandardScaler
import pandas as pd
import plotly.express as px
import plotly.graph_objects as go

# Carregar os dados
ds = pd.read_csv("Social_Network_Ads.csv")

# Selecionar features e alvo
X = ds[["Age", "EstimatedSalary"]]
y = ds["Purchased"]  # Apenas uma coluna para o alvo

# Padronizar os dados
sc = StandardScaler()
X = sc.fit_transform(X)

# Dividir o conjunto de dados em treino e teste
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)

# Inicializar e treinar o modelo SVM
# svm = SVC(kernel='linear', random_state=1, C=2.0)
# svm.fit(X_train, y_train)

svm = SVC(kernel='rbf', random_state=1, C=2.0)
svm.fit(X_train, y_train)

# svm = SVC(kernel='sigmoid', random_state=1, C=1.0)
# svm.fit(X_train, y_train)

# Fazer previsões no conjunto de teste
y_pred = svm.predict(X_test)

# Calcular a acurácia
acuracia = accuracy_score(y_true=y_test, y_pred=y_pred)
print("Acurácia:", acuracia)


x_min, x_max = X[:, 0].min() - 1, X[:, 0].max() + 1
y_min, y_max = X[:, 1].min() - 1, X[:, 1].max() + 1
xx, yy = np.meshgrid(np.arange(x_min, x_max, 0.01),
                     np.arange(y_min, y_max, 0.01))
Z = svm.predict(np.c_[xx.ravel(), yy.ravel()])
Z = Z.reshape(xx.shape)

# Plotar os dados de treino
fig = px.scatter(x=X_train[:, 0], y=X_train[:, 1], color=y_train)
fig.update_traces(marker=dict(size=12))
fig.update_layout(title="Conjunto de Treinamento", xaxis_title="Idade", yaxis_title="Salário Estimado")


# Adicionar as fronteiras de decisão
fig.add_trace(go.Contour(x=np.arange(x_min, x_max, 0.01),
                          y=np.arange(y_min, y_max, 0.01),
                          z=Z,
                          colorscale='Viridis',
                          showscale=False,
                          opacity=0.5,
                          name='Fronteira de Decisão'))


# Plotar os dados de teste
fig.add_trace(go.Scatter(x=X_test[:, 0], y=X_test[:, 1], mode='markers', marker=dict(size=10, color='black'), name='Conjunto de Teste'))

fig.show()