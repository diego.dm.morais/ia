import numpy as np
from sklearn.preprocessing import StandardScaler
from sklearn.cluster import KMeans
import plotly.express as px
import plotly.graph_objects as go

idade = np.random.randint(20, 50, 100)
salario = np.random.uniform(2700, 6000, 100)


dados = np.column_stack((idade, salario))

scaler_salario = StandardScaler()
base_salario = scaler_salario.fit_transform(dados)


k_Means = KMeans(n_clusters=4)
k_Means.fit(base_salario)

centroide = k_Means.cluster_centers_
scaler_salario.inverse_transform(centroide)

rotolos = k_Means.labels_

tamanho = [8] * len(centroide)

grafico = px.scatter(x=base_salario[:,0], y=base_salario[:,1], color=rotolos)
grafico2 = px.scatter(x=centroide[:, 0], y=centroide[:, 1], size=tamanho)
grafico3 = go.Figure(data=grafico.data + grafico2.data)
grafico3.show()
