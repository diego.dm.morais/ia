// contexts/QuestionContext.tsx
import React, { createContext, useContext, useState, ReactNode } from 'react';

type Answer = {
  questionId: number;
  answer: number;
};

type QuestionContextType = {
  answers: Answer[];
  setAnswer: (answer: Answer) => void;
};

const QuestionContext = createContext<QuestionContextType | undefined>(undefined);

export const useQuestionContext = () => {
  const context = useContext(QuestionContext);
  if (!context) {
    throw new Error('useQuestionContext must be used within a QuestionProvider');
  }
  return context;
};

type QuestionProviderProps = {
  children: ReactNode;
};

export const QuestionProvider: React.FC<QuestionProviderProps> = ({ children }) => {
  const [answers, setAnswers] = useState<Answer[]>([]);

  const setAnswer = (answer: Answer) => {
    setAnswers((prevAnswers) => [...prevAnswers.filter(a => a.questionId !== answer.questionId), answer]);
  };

  return (
    <QuestionContext.Provider value={{ answers, setAnswer }}>
      {children}
    </QuestionContext.Provider>
  );
};
