
import React from 'react';

interface QuestionProps {
  question: string;
  children: React.ReactNode;
  onNext: () => void;
  onPrevious?: () => void;
  title?: string;
}

const Question: React.FC<QuestionProps> = ({ title = "Questão", question, children, onNext, onPrevious }) => {
  return (
    <div className="flex flex-col items-center justify-center min-h-screen p-4 bg-gray-100">
      <div className="bg-white p-6 rounded-lg shadow-lg w-full max-w-md">
        <h1 className="text-3xl font-bold mb-4 text-gray-700 uppercase">{title}</h1>
        <h2 className="text-2xl font-light mb-4 text-gray-700 text-justify">{question}</h2>
        <div className="mb-4">{children}</div>
        <div className="flex justify-between">
          {onPrevious && (
            <button
              className="bg-gray-300 text-white py-2 px-10 rounded hover:bg-gray-400"
              onClick={onPrevious}
            >
              Voltar
            </button>
          )}
          <button
            className="bg-purple-500 text-white py-2 px-10 rounded hover:bg-purple-700 ml-auto"
            onClick={onNext}
          >
            Próximo
          </button>
        </div>
      </div>
    </div>
  );
};

export default Question;
