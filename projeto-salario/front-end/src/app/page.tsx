'use client'

import { useRouter } from 'next/navigation'
import { useEffect, useState } from 'react';
import { FcOk, FcHighPriority } from 'react-icons/fc';

export default function Page() {
  const router = useRouter()
  const [status, setStatus] = useState<boolean>(false);

  const startSurvey = () => {
    router.push("/question_1");
  };

  useEffect(() => {
    const checkHealth = async () => {
      try {
        const response = await fetch('/api/check_health');
        const data = await response.json();
        setStatus(data.status === "success");
      } catch (error) {
        setStatus(false);
      }
    };

    checkHealth();
  }, []);

  return (
    <>
      <div className="relative">
        <div className='absolute top-0 right-0 m-4'>
          {status === true && <FcOk className="w-5 h-5 text-gray-700" />}
          {status === false && <FcHighPriority className="w-5 h-5 text-gray-700" />}
        </div>
      </div>
      <div className="flex flex-col items-center justify-center min-h-screen p-4 bg-gray-100">
        <div className="p-6 rounded-lg shadow-lg w-full max-w-md">
          <h1 className="text-2xl font-semibold mb-4 text-gray-700 uppercase">Crafting an Attractive Salary Proposal for the Candidate</h1>
          <h2 className="text-xl font-light text-justify mb-4 text-gray-700">To provide a more precise salary estimate through artificial intelligence, it's crucial to understand the candidate's profile and skills. By answering these questions, the AI can estimate the average salary of a software developer in Brazil more accurately.</h2>
          <button
            className="bg-purple-500 text-white py-2 px-10 rounded hover:bg-purple-700 ml-auto float-end"
            onClick={startSurvey}
          >
            Starting
          </button>
        </div>
      </div>
    </>
  );
}