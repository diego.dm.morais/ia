// src/app/questions/Question1.tsx

import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import Question from '@/app/components/Question';
import "@/app/globals.css";
import { useQuestionContext } from '@/contexts/QuestionContext';

const Question1: React.FC = () => {
  const router = useRouter();
  const [value, setValue] = useState<number>(0);

  const { answers, setAnswer } = useQuestionContext();
  const onGoTo = (route: string) => {
    setAnswer({ questionId: 6, answer: value });
    router.push(route)
  }

  useEffect(() => {
    const found = answers.find(it => it.questionId === 6)
    if (found) {
      setValue(found!.answer)
    }
  },[])

  return (
    <Question
      title='Question 6'
      // question="Em uma escala de 0 a 10, qual é o seu nível de habilidade em banco de dados, incluindo SQL, NoSQL (como MongoDB), PostgreSQL, Oracle, MySQL, entre outros?"
      question="On a scale of 0 to 10, what is the candidate's level of proficiency in database skills, including SQL, NoSQL (such as MongoDB), PostgreSQL, Oracle, MySQL, among others?"
      onNext={() => onGoTo('/question_7')}
      onPrevious={() => onGoTo('/question_5')}
    >
      <input
        type="range"
        min={0}
        max={10}
        value={value}
        onChange={(e) => setValue(Number(e.target.value))}
        className="w-full h-1 mb-6 bg-gray-200 rounded-lg appearance-none cursor-pointer range-sm dark:bg-gray-500"

      />
      <p className='text-xl font-light mb-4 text-gray-700 text-justify' >Response: {value}</p>

    </Question>
  );
};

export default Question1;
