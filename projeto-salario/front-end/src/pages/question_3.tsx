// src/app/questions/Question1.tsx

import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import Question from '@/app/components/Question';
import "@/app/globals.css";
import { useQuestionContext } from '@/contexts/QuestionContext';

const Question1: React.FC = () => {
  const router = useRouter();
  const [option, setOption] = useState<string>('');

  const handleOptionChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setOption(e.target.value);
  };

  const { answers, setAnswer } = useQuestionContext();
  const onGoTo = (route: string) => {
    setAnswer({ questionId: 3, answer: (option === "Yes" ? 1 : 0) });
    router.push(route)
  }


  useEffect(() => {
    const found = answers.find(it => it.questionId === 3)
    if (found) {
      setOption(found!.answer === 1 ? "Yes" : "No")
    }
  }, [])

  return (
    <Question
      title='Question 3'
      // question="O candidato obteve um diploma de pós-graduação ou completou com sucesso um programa de estudos avançados após a conclusão de um curso de graduação em uma instituição educacional reconhecida?"
      question="Has the candidate earned a postgraduate diploma or successfully completed an advanced studies program following the completion of an undergraduate course at a recognized educational institution?"
      onNext={() => onGoTo('/question_4')}
      onPrevious={() => onGoTo('/question_2')}
    >
      <div className='p-2 w-full text-black'>
        <div className='flex flex-row w-full text-black'>
          <label className='inline-flex items-center'>
            <input
              className='text-gray-700 m-2 '
              type="radio"
              value="Yes"
              checked={option === 'Yes'}
              onChange={handleOptionChange}
            />
            <span className='text-xl font-light text-gray-700'>Yes</span>
          </label>

          <label className='inline-flex items-center'>
            <input
              className='text-gray-700 m-2'
              type="radio"
              value="No"
              checked={option === 'No'}
              onChange={handleOptionChange}
            />
            <span className='text-xl font-light text-gray-700'>No</span>
          </label>
        </div>
      </div>

    </Question>
  );
};

export default Question1;
