// src/app/questions/Question1.tsx

import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import Question from '@/app/components/Question';
import { useQuestionContext } from '@/contexts/QuestionContext';

import "@/app/globals.css";

const Question1: React.FC = () => {
  const [value, setValue] = useState<number>(0);
  const router = useRouter();

  const { answers, setAnswer } = useQuestionContext();
  const onGoTo = (route: string) => {
    setAnswer({ questionId: 1, answer: value });
    router.push(route)
  }

  useEffect(() => {
    const found = answers.find(it => it.questionId === 1)
    if (found) {
      setValue(found.answer)
    }
  },[])

  return (
    <Question
      title='QUESTION 1'
      // question="Quantos anos de experiência o candidato tem em desenvolvimento de software?"
      question="How many years of experience does the candidate have in software development?"
      onNext={() => onGoTo('/question_2')}
    >
      <input
        type="range"
        min={0}
        max={20}
        value={value}
        onChange={(e) => setValue(Number(e.target.value))}
        className="w-full h-1 mb-6 bg-gray-200 rounded-lg appearance-none cursor-pointer range-sm dark:bg-gray-500"

      />
      <p className='text-xl font-light mb-4 text-gray-700 text-justify' >Response: {value}</p>
    </Question>
  );
};

export default Question1;
