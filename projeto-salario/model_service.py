from ast import Str
import os
import pandas as pd
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.model_selection import train_test_split
from sklearn.metrics import r2_score
from sklearn.preprocessing import StandardScaler
import joblib

FILENAME_MODEL = os.path.join("model", "gradient_boosting_model.pkl")
FILENAME_SCALER = os.path.join("model", "scaler.pkl")
FILENAME_CSV = os.path.join("csv", "job_level.csv")


REQUIRED_FIELDS_PREDICT = {
    "tempo_experiencia": int,
    "faculdade_concluida_0_1": int,
    "pos_graduado_0_1": int,
    "conhecimento_programacao_0_10": int,
    "conheciemento_das_ferramentas_trabalho_0_10": int,
    "conhecimento_banco_de_dados_0_10": int,
    "conhecimento_back_end_0_10": int,
    "conhecimento_front_end_0_10": int,
    "conhecimento_devops_0_10": int,
    "communicacao_0_10": int,
    "dominio_sistemas_operacionais_0_5": int,
    "conhecimento_basico_rede_0_5": int,
    "lideranca_0_10": int,
}

REQUIRED_FIELDS = {
    **REQUIRED_FIELDS_PREDICT,
    "salario": float,
}


class ModeloService:

    @classmethod
    def add_instance(cls, data: dict) -> None:
        cls._validate(data, REQUIRED_FIELDS)
        df = pd.DataFrame({key: [value] for key, value in data.items()})
        try:
            existing_df = pd.read_csv(FILENAME_CSV)
            df = pd.concat([existing_df, df], ignore_index=True)
        except FileNotFoundError:
            pass
        df.to_csv(FILENAME_CSV, index=False)

    @classmethod
    def test_model_fixo(cls) -> float:
        model = joblib.load(FILENAME_MODEL)
        scaler = joblib.load(FILENAME_SCALER)

        nova_instancia = pd.DataFrame(
            {
                "tempo_experiencia": [12],
                "faculdade_concluida_0_1": [1],
                "pos_graduado_0_1": [1],
                "conhecimento_programacao_0_10": [10],
                "conheciemento_das_ferramentas_trabalho_0_10": [10],
                "conhecimento_banco_de_dados_0_10": [10],
                "conhecimento_back_end_0_10": [10],
                "conhecimento_front_end_0_10": [10],
                "conhecimento_devops_0_10": [10],
                "communicacao_0_10": [10],
                "dominio_sistemas_operacionais_0_5": [5],
                "conhecimento_basico_rede_0_5": [5],
                "lideranca_0_10": [10],
            }
        )

        nova_instancia_scaled = scaler.transform(nova_instancia)

        # Fazer a previsão
        previsao = model.predict(nova_instancia_scaled)
        return float(previsao[0])

    @classmethod
    def update_model(cls) -> None:
        # Carregar os dados do CSV
        df = pd.read_csv(FILENAME_CSV)

        # Separar features do target/label
        X = df.drop(columns=["salario"], axis=1)
        y = df["salario"]

        # Dividir os dados em conjuntos de treinamento e teste
        X_train, X_test, y_train, y_test = train_test_split(
            X, y, test_size=0.2, random_state=42
        )

        # Normalizar os dados
        scaler = StandardScaler()
        X_train_scaled = scaler.fit_transform(X_train)
        X_test_scaled = scaler.transform(X_test)

        # Criar um modelo usando Gradient Boosting
        model = GradientBoostingRegressor(
            n_estimators=150, learning_rate=0.1, max_depth=4, random_state=42
        )
        model.fit(X_train_scaled, y_train)
        y_pred = model.predict(X_test_scaled)
        r2 = r2_score(y_test, y_pred)
        print(f"Gradient Boosting R^2: {r2}")
        if r2 > 0.8:
            # Salvar o modelo e o scaler
            cls._ensure_directory_exists("model")
            joblib.dump(model, FILENAME_MODEL)
            joblib.dump(scaler, FILENAME_SCALER)
        else:
            raise Exception("Very low score")

    @classmethod
    def predict(cls, data: dict) -> dict:
        cls._validate(data, REQUIRED_FIELDS_PREDICT)
        model = joblib.load(FILENAME_MODEL)
        scaler = joblib.load(FILENAME_SCALER)
        nova_instancia = pd.DataFrame({key: [value] for key, value in data.items()})
        nova_instancia_scaled = scaler.transform(nova_instancia)
        previsao = model.predict(nova_instancia_scaled)
        return {"label": float(previsao[0])}

    @classmethod
    def _ensure_directory_exists(cls, directory):
        if not os.path.exists(directory):
            os.makedirs(directory)

    @classmethod
    def _validate(cls, data: dict, required_fields: dict) -> None:
        for field, field_type in required_fields.items():
            if field not in data:
                raise ValueError(f"The required field '{field}' is missing.")
            if not isinstance(data[field], field_type):
                raise TypeError(
                    f"The field '{field}' must be of type {field_type.__name__}."
                )
