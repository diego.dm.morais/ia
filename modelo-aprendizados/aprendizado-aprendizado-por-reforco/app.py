import gym
from stable_baselines3 import DQN

# Criar o ambiente personalizado
class SimpleGridEnv(gym.Env):
    def __init__(self):
        super(SimpleGridEnv, self).__init__()
        self.observation_space = gym.spaces.Discrete(4)  # 4 estados
        self.action_space = gym.spaces.Discrete(2)  # 2 ações: 0 (esquerda) e 1 (direita)
        self.grid = [0, 0, 1, 2]  # Ambiente de grade simples
        self.goal_state = 3

    def reset(self):
        self.agent_position = 0  # Posição inicial do agente
        return self.agent_position

    def step(self, action):
        # Executa a ação no ambiente
        if action == 0:  # Esquerda
            self.agent_position = max(0, self.agent_position - 1)
        elif action == 1:  # Direita
            self.agent_position = min(len(self.grid) - 1, self.agent_position + 1)

        # Calcula a recompensa
        reward = 100 if self.agent_position == self.goal_state else -1

        # Verifica se o episódio terminou
        done = self.agent_position == self.goal_state

        return self.agent_position, reward, done, {}

# Criar o ambiente
env = SimpleGridEnv()

# Criar o modelo DQN
model = DQN("MlpPolicy", env, verbose=1)

# Treinar o modelo
model.learn(total_timesteps=10000)

# Testar o modelo
obs = env.reset()
for _ in range(20):
    action, _ = model.predict(obs, deterministic=True)
    obs, reward, done, _ = env.step(action)
    print(f"Estado: {obs}, Recompensa: {reward}")
    if done:
        break
