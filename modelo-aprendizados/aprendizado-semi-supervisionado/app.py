# Importando bibliotecas necessárias
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import accuracy_score

# Gerando dados fictícios de exemplo
# Suponha que 'X' são os recursos (por exemplo, preço das ações, volume de negociação) 
# e 'y' são os rótulos (0 para vender, 1 para comprar)
X_labeled = np.array([[100, 1000], [110, 1200], [90, 800]])
y_labeled = np.array([0, 1, 0])

X_unlabeled = np.array([[95, 900], [105, 1100]])

# Dividindo os dados rotulados em conjuntos de treinamento e teste
X_train_labeled, X_test_labeled, y_train_labeled, y_test_labeled = train_test_split(X_labeled, y_labeled, test_size=0.2, random_state=42)

# Combinando dados rotulados e não rotulados
X_combined = np.concatenate((X_train_labeled, X_unlabeled), axis=0)

# Criando e treinando o modelo de classificação semi-supervisionado (KNN)
model = KNeighborsClassifier(n_neighbors=3)
model.fit(X_combined, np.concatenate((y_train_labeled, [-1, -1])))

# Fazendo previsões no conjunto de teste (dados rotulados)
predictions_labeled = model.predict(X_test_labeled)

# Calculando a acurácia do modelo nos dados rotulados
accuracy_labeled = accuracy_score(y_test_labeled, predictions_labeled)
print("Acurácia do modelo nos dados rotulados:", accuracy_labeled)

# Exemplo de previsão para novos dados (dados não rotulados)
predictions_unlabeled = model.predict(X_unlabeled)
print("Previsões para os dados não rotulados:", predictions_unlabeled)


# Exemplo de previsão para novos dados não rotulados
new_data_unlabeled = np.array([[120, 1300], [100, 900]])  # Novos dados de preço e volume
predictions_unlabeled = model.predict(new_data_unlabeled)

for i, prediction in enumerate(predictions_unlabeled):
    if prediction == 0:
        print(f"Para os dados {new_data_unlabeled[i]}: Recomendação: Vender")
    else:
        print(f"Para os dados {new_data_unlabeled[i]}: Recomendação: Comprar")  