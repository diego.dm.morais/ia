# Importando bibliotecas necessárias
import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans

# Gerando dados fictícios de exemplo
# Suponha que 'X' são os dados de preço das ações ao longo de vários dias
X = np.array([[100], [110], [90], [95], [105], [120], [130], [125], [140]])

# Criando o modelo K-Means com 2 clusters (poderíamos experimentar com outros valores)
kmeans = KMeans(n_clusters=2)
kmeans.fit(X)

# Obtendo os rótulos dos clusters e os centróides
labels = kmeans.labels_
centroids = kmeans.cluster_centers_

# Plotando os dados e os centróides
plt.scatter(X, np.zeros_like(X), c=labels, cmap='viridis', alpha=0.5)
plt.scatter(centroids, np.zeros_like(centroids), marker='X', c='red', s=200)
plt.xlabel('Preço das ações')
plt.title('Clustering dos preços das ações')
plt.show()
