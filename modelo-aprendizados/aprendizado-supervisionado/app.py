# Importando bibliotecas necessárias
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score

# Gerando dados fictícios de exemplo
# Suponha que 'X' são os recursos (por exemplo, preço das ações, volume de negociação) 
# e 'y' são os rótulos (0 para vender, 1 para comprar)
X = np.array([[100, 1000], [110, 1200], [90, 800], [95, 900], [105, 1100]])
y = np.array([0, 1, 0, 0, 1])

# Dividindo os dados em conjuntos de treinamento e teste
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Criando e treinando o modelo de regressão logística
model = LogisticRegression()
model.fit(X_train, y_train)

# Fazendo previsões no conjunto de teste
predictions = model.predict(X_test)

# Calculando a acurácia do modelo
accuracy = accuracy_score(y_test, predictions)
print("Acurácia do modelo:", accuracy)

# Exemplo de previsão para novos dados
new_data = np.array([[120, 1300]])  # Novos dados de preço e volume
prediction = model.predict(new_data)
if prediction == 0:
    print("Recomendação: Vender")
else:
    print("Recomendação: Comprar")
