import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.linear_model import LogisticRegression
import numpy as np
import pandas as pd

df = pd.read_csv("https://pycourse.s3.amazonaws.com/temperature.csv")

x, y = df[['temperatura']].values, df[["classification"]].values

print(f'x: valor linear\n{x}')
print(f'y: valor categorigo ou categorização \n{y}')

le = LabelEncoder()
y_t = le.fit_transform(y.ravel())
print(f'y_t:\n{y_t}')
print('--------------------------------------------------------------\n')

clf = LogisticRegression()
clf.fit(x, y)


# gerando valores aleatorio para fazer o nosso teste
x_test = np.linspace(start=0., stop=45., num=100).reshape(-1, 1)
print(f'x_test:\n{x_test}')
print('--------------------------------------------------------------\n')

# como base nas classificações mostra quais são as classificações
y_predic = clf.predict(x_test)
print(f'y_predic:\n{y_predic}')
print(f'score:\n{clf.score(x,y)}')
print('--------------------------------------------------------------\n')

y_trans = le.transform(y_predic)
print(f'y_trans:\n{y_trans}')
print('--------------------------------------------------------------\n')

y_trans_i = le.inverse_transform(y_trans)
print(f'y_trans_i:\n{y_trans_i}')
print('--------------------------------------------------------------\n')

data = {
    'new_temp': x_test.ravel(),
    'new_class': y_predic.ravel()
}

output = pd.DataFrame(data)
print(f'data:\n{output.head()}')
print('--------------------------------------------------------------\n')
print(f'describe:\n{output.describe()}')
print('--------------------------------------------------------------\n')
print(f'describe:\n{output.info()}')


# def classify_temp():
#     ask = True
#     while ask:
#         temp = input("insira sua temperatura: ")
#         temp = np.array(float(temp)).reshape(-1, 1)
#         class_temp = clf.predict(temp)
#         print(
#             f'A classificação da temperatura {temp.ravel()[0]} é: {class_temp[0]}')
#         temp = input("Nova classificação (y/n):")
#         ask = temp == "y"


# classify_temp()
