import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# Create sample data (replace with your actual data source)
dates = pd.date_range('2023-01-01', '2023-01-30')
open_prices = np.random.uniform(10, 19, size=len(dates))
high_prices = np.random.uniform(open_prices, 20, size=len(dates))
low_prices = np.random.uniform(9, open_prices, size=len(dates))
close_prices = np.random.uniform(low_prices, high_prices, size=len(dates))

data = pd.DataFrame({
    'Date': dates,
    'Open': open_prices,
    'High': high_prices,
    'Low': low_prices,
    'Close': close_prices
})

# Média móvel simples de 10 períodos
def sma(data, window_size):
    return np.convolve(data['Close'], np.ones(window_size), 'valid') / window_size

# Calculando a SMA
sma10 = sma(data, 10)

# Níveis de suporte e resistência
suporte = 12
resistencia = 15

# Define candle colors based on price movement
def candle_color(open_price, close_price):
    if close_price > open_price:
        return 'green'
    elif close_price < open_price:
        return 'red'
    else:
        return 'black'

data['Color'] = data.apply(lambda row: candle_color(row['Open'], row['Close']), axis=1)

# Create the candlestick chart
plt.figure(figsize=(12, 6)) # Adjust figure size as needed

# Candlestick bodies
plt.bar(data['Date'], data['Close'] - data['Open'], bottom=data['Open'], color=data['Color'])

plt.plot(data['Date'][9:], sma10, color='blue', label='SMA10')

# Upper and lower wicks (lines)
plt.vlines(x=data['Date'], ymin=data['Low'], ymax=data['High'], color='black')  # Corrected line

# Optional: Add volume bars (replace with your volume data)
# volumes = np.random.randint(10000, 50000, size=len(dates))
# plt.bar(data['Date'], volumes, bottom=0, color='gray', alpha=0.3, label='Volume')

# Customize axes labels and title
plt.xlabel('Date')
plt.ylabel('Price')
plt.title('Candlestick Chart')

# Rotate x-axis labels for better readability with many dates
plt.xticks(rotation=45)

# Add grid for clarity
plt.grid(True, which='both', linestyle='--', linewidth=0.5)

# Optional: Add legend (if volume bars are included)
# plt.legend()

plt.tight_layout()
plt.show()
