import pandas as pd
import numpy as np

print(f'__________________code 1______________________\n')
Z = np.zeros((4,))
print(f'Z: {Z}')

print(f'\n________________code 2________________________\n')
Z = np.zeros((4,))
Z[1] = 1.
print(f'Z: {Z}')

print(f'\n________________code 3________________________\n')
Z = np.zeros((4,))
Z[1:] = 1.
print(f'Z: {Z}')

print(f'\n________________code 4________________________\n')
Z = np.zeros((4,))
Z[:-1] = 1.
print(f'Z: {Z}')

print(f'\n________________code 5.1________________________\n')
X = np.ones((2, 2)) * 2
print(f'X: {X}')

# X = np.twos((2, 2))
# print(f'X: {X}')

X = np.array([2.] * 4).reshape(2, 2)
print(f'X: {X}')

X = 2 * np.ones((2, 2))
print(f'X: {X}')

X = np.ones((2, 2)) + np.ones((2, 2))
print(f'X: {X}')

print(f'\n________________code 5.2________________________\n')
X = np.array([[2., 2.], [2., 2.]])
print(f'X: {X}')

print(f'\n________________code 6________________________\n')
X = np.array([[1, 2], [3, 4]])
Y = X[0, :]
Y[1] = 10
print(f'Y: {X}')

print(f'\n________________code 7________________________\n')
X = np.array([[1, 3], [11, 10]])
Y = np.mean(X[X > np.pi])
print(f'Y: {Y}')

print(f'\n________________code 8 e 9________________________\n')
data = {
    'animal': ['cat', 'cat', 'snake', 'dog', 'dog', 'cat', 'snake', 'cat', 'dob', 'dog'],
    'age': [2.5, 3, 0.5, np.nan, 5, 2, 4.5, np.nan, 7, 3],
    'visits': [1, 3, 2, 3, 2, 3, 1, 1, 2, 1],
    'priority': ['yes', 'yes', 'no', 'yes', 'no', 'no', 'no', 'yes', 'no', 'no']
}
labels = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j']

df = pd.DataFrame(data=data, index=labels)
print(df)
print(f'shape quantidade de linhas e colunas: {df.shape}')

dfc = df.copy()
print(dfc['animal'].value_counts())

age = dfc['age'].value_counts().mean()
print(f'media de idade: {age}')
print(f'\n________________________________________\n')
print(f'medias:\n {df.describe()}')
print(f'\n________________________________________\n')
print(df.iloc[:, 3])
# print(df.loc[:, 'visits'])
# print(df['visits'])
# print(df.iloc[:, -2])
order = df.sort_values(by='visits', ascending=False)
print(f'ordenado:\n {order}')

print(f'\n________________code 10________________________\n')
y_true = np.array([1., 2., 1.])
y_pred = np.array([1.1, 1.98, 1.05])
print(f'y_true: {y_true}')
print(f'y_pred: {y_pred}')

a = y_true-y_pred
print(f'a:{a}')
b = a**2
print(f'b:{b}')
c = np.sqrt(b.mean())
print(f'c:{c}')


print(f'\n________________atividade 4________________________\n')
# Z: [1. 1. 1. 0.]
# Z = np.ones((4,))
# Z[4] = 0.
# print(f'Z: {Z}')

Z = np.zeros((4, ))
Z[:-1] = 1.
print(f'Z: {Z}')

Z = np.ones((4,))
Z[-1] = 0.
print(f'Z: {Z}')

Z = np.zeros((4, ))
Z[:3] = 1.
print(f'Z: {Z}')
