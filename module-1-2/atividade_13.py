import matplotlib
matplotlib.use('TkAgg')

from matplotlib import pyplot as plt
import numpy as np
from sklearn.linear_model import LinearRegression

x = [-1, -0.77, -0.55, -0.33, -0.11, 0.11, 0.33, 0.55, 0.77, 1]
y = [-1.13, -0.57, -0.21, 0.54, 0.49, 1.14, 1.64, 2.17, 2.64, 2.95]
print(x)
print(y)


x, y = np.array(x).reshape(-1,1), np.array(y).reshape(-1,1)

lr = LinearRegression()
lr.fit(x,y)

print(f'a estimado:{lr.coef_.ravel()[0]}')
print(f'b estimado:{lr.intercept_.ravel()[0]}')

y_pred = lr.predict(x)
score = lr.score(x,y)
print(f'score:{score}')


# # convertendo em matriz
# x, y = np.array(x).reshape(-1,1), np.array(y).reshape(-1,1)

# print(x)
# print(y)

# # adicionado uma coluna com "1.""
# X = np.hstack((x, np.ones(x.shape)))

# print(X)
# print(y)

# "calculando (((x ** t) * x) ** -1) * (x ** t)  "
# beta = np.linalg.pinv(X).dot(y)
# print(beta)

# print("_______________________________________________________________\n")
# X = X.dot(beta)
# print(beta)
# print("---------------------------------------------------------------\n")
# print(X)
# print("---------------------------------------------------------------\n")
# print(x)
# print("\n_______________________________________________________________")



plt.figure(figsize=(10, 5))
plt.plot(x,y, 'o--', label='Dados originais')
plt.plot(x,y_pred, label='Regressão linear (R2: {:.3f})'.format(score))
plt.legend()
plt.xlabel("x")
plt.ylabel("y")
plt.grid()
plt.show()


