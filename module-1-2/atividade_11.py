import pandas as pd
import matplotlib.pyplot as plt
import requests

# Função para obter os dados do histórico do Bitcoin
def get_bitcoin_data(start_date='2009-01-01'):
    url = f'https://api.coindesk.com/v1/bpi/historical/close.json?start={start_date}&end=today'
    response = requests.get(url)
    data = response.json()
    df = pd.DataFrame(data['bpi']).T
    df.index = pd.to_datetime(df.index)
    return df

# Função para gerar gráficos de candles
def plot_candles(df):
    fig, ax = plt.subplots(figsize=(12, 6))
    ax.plot(df.index, df['close'], label='Close')
    ax.fill_between(df.index, df['close'], color='blue', alpha=0.3)
    ax.set_xlabel('Date')
    ax.set_ylabel('Price')
    ax.set_title('Bitcoin Price History')
    ax.legend()
    plt.show()

# Obter os dados do histórico do Bitcoin
df = get_bitcoin_data()

# Gerar gráficos de candles
plot_candles(df)