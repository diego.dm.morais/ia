import numpy as np
import pandas as pd

df = pd.read_csv("bike-sharing.csv")

print(f'{df.head()}\n')
# 4. Responder às seguintes perguntas:
# a. Qual o tamanho desse dataset?
num_rows, num_cols = df.shape
dataset_size = num_rows * num_cols
print(f'R. a) numero de linhas: {num_rows}.\n numero de colunas: {num_cols}.\n')
print(f'R. a) Tamanho do dataset 1: {df.size}.\n')
print(f'R. a) Tamanho do dataset 2: {dataset_size}.\n')

# b. Qual a média da coluna windspeed?
mean_windspeed = df['windspeed'].mean()
print(f'R. b) Média da coluna windspeed: {mean_windspeed}\n')

# c. Qual a média da coluna temp?
mean_temp = df['temp'].mean()
print(f'R. c) Média da coluna temp: {mean_temp}\n')

# d. Quantos registros existem para o ano de 2011?
count = df.loc[df['year'] == 0, 'year'].count()
print(f'R. d) Existem para o ano de 2011: {count}\n')

# e. Quantos registros existem para o ano de 2012?
count = df.loc[df['year'] == 1, 'year'].count()
print(f'R. e) Existem para o ano de 2012: {count}\n')

# f. Quantas locações de bicicletas foram efetuadas em 2011? 247252
sum_casual = df.loc[df['year'] == 0, 'total_count'].sum()
print(f'R. f) Locações de bicicletas foram efetuadas em 2011: {sum_casual}\n')

# g. Quantas locações de bicicletas foram efetuadas em 2012?
sum_casual = df.loc[df['year'] == 1, 'total_count'].sum()
print(f'R. g) Locações de bicicletas foram efetuadas em 2012: {sum_casual}\n')

# h. Qual estação do ano contém a maior média de locações de bicicletas?
avg_counts = df.groupby('season')['total_count'].mean()
max_season = avg_counts.idxmax()
print(f'R. h) Estação do ano contém a maior média de locações de bicicletas: {max_season}\n')
# print(f'{avg_counts}\n')

# i. Qual estação do ano contém a menor média de locações de bicicletas?
avg_counts = df.groupby('season')['total_count'].mean()
min_season = avg_counts.idxmin()
print(f'R. i) Estação do ano contém a menor média de locações de bicicletas: {min_season}\n')
# print(f'{avg_counts}\n')

# j. Qual horário do dia contém a maior média de locações de bicicletas?
avg_counts = df.groupby('hour')['total_count'].mean()
max_hour = avg_counts.idxmax()
print(f'R. j) Horário do dia contém a maior média de locações de bicicletas: {max_hour}h\n')
# print(f'{avg_counts}\n')

# k. Qual horário do dia contém a menor média de locações de bicicletas?
avg_counts = df.groupby('hour')['total_count'].mean()
min_weekday = avg_counts.idxmin()
print(f'R. k) Horário do dia contém a menor média de locações de bicicletas: {min_weekday}h\n')
# print(f'{avg_counts}\n')

# l. Que dia da semana contém a maior média de locações de bicicletas?
avg_counts = df.groupby('weekday')['total_count'].mean()
min_weekday = avg_counts.idxmax()
print(f'R. l) Dia da semana contém a maior média de locações de bicicletas: {min_weekday}\n')
# # print(f'{avg_counts}\n')

# m. Que dia da semana contém a menor média de locações de bicicletas?
avg_counts = df.groupby('weekday')['total_count'].mean()
min_weekday = avg_counts.idxmin()
print(f'R. m) Dia da semana contém a menor média de locações de bicicletas: {min_weekday}\n')
# print(f'{avg_counts}\n')

# n. Às quartas-feiras (weekday = 3), qual o horário do dia que contém a maior média de locações de bicicletas?
df_mean_day = df.loc[df['weekday']==3, ['total_count', 'hour']]
avg_counts = df_mean_day.groupby('hour')['total_count'].mean()
max_weekday = avg_counts.idxmax()
print(f'R. n) Às quartas-feiras (weekday = 3), qual o horário do dia que contém a maior média de locações de bicicletas: {max_weekday}h\n')
# print(f'{avg_counts}\n')

# o. Aos sábados (weekday = 6), qual o horário do dia que contém a maior média de locações de bicicletas?
df_mean_day = df.loc[df['weekday']==6, ['total_count', 'hour']]
avg_counts = df_mean_day.groupby('hour')['total_count'].mean()
max_weekday = avg_counts.idxmax()
print(f'R. o) Aos sábados (weekday = 6), qual o horário do dia que contém a maior média de locações de bicicletas: {max_weekday}h\n')
# print(f'{avg_counts}\n')