import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter

# Sample data
data = {'Date': ['2023-01-01', '2023-01-02', '2023-01-03', '2023-01-04', '2023-01-05'],
        'Open': [64319.0, 64305.2, 64349.7, 64337.1, 64305.1],
        'High': [64324.1, 64349.8, 64349.8, 64353.3, 64305.1],
        'Low': [64269.4, 64349.8, 64337.1, 64305.0, 64284.5],
        'Close': [64315.8, 64330.4, 64334.2, 64351.2, 64297.8]}

# Convert to DataFrame
df = pd.DataFrame(data)

# Convert 'Date' column to datetime
df['Date'] = pd.to_datetime(df['Date'])

# Define candle colors based on price movement
def candle_color(open_price, close_price):
    if close_price > open_price:
        return 'green'
    elif close_price < open_price:
        return 'red'
    else:
        return 'black'

df['Color'] = df.apply(lambda row: candle_color(row['Open'], row['Close']), axis=1)

# Create the candlestick chart
plt.figure(figsize=(10, 5)) # Adjust figure size as needed

# Candlestick bodies (adjusting width)
plt.bar(df['Date'], df['Close'] - df['Open'], bottom=df['Open'], color=df['Color'], width=0.6)

# Upper and lower wicks (lines with specified width)
plt.vlines(x=df['Date'], ymin=df['Low'], ymax=df['Open'], color='black', linewidth=1)  # Lower wick
plt.vlines(x=df['Date'], ymin=df['Close'], ymax=df['High'], color='black', linewidth=1)  # Upper wick

# Customize axes labels and title
plt.xlabel('Date')
plt.ylabel('Price')
plt.title('Candlestick Chart')

# Rotate x-axis labels for better readability with many dates
plt.xticks(rotation=45)

# Add grid for clarity
plt.grid(True, which='both', linestyle='-.', linewidth=0.5)

# Format y-axis to display float values
def format_price(value, _):
    return f'{value:.1f}'

plt.gca().yaxis.set_major_formatter(FuncFormatter(format_price))

plt.tight_layout()
plt.show()
