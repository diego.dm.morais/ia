from audioop import lin2adpcm
from turtle import color
from numpy import rot90
import pandas as pd
from pyparsing import line_end

df = pd.read_csv("https://pycourse.s3.amazonaws.com/temperature.csv")
# print(df.head(3))

# print(df[['classification','date']])


# print("-----------------------------------------------")
# print(df[["date", "temperatura" ,"classification"]])
# print("_______________________________________________")
# print(df.iloc[:,1] )
# print("-----------------------------------------------")

# print("-----------------------------------------------")
# print(df[["date", "temperatura" ,"classification"]])
# print("_______________________________________________")
# print(df.iloc[-1,-1] )
# print("-----------------------------------------------")

# print("-----------------------------------------------")
# print(df[["date", "temperatura" ,"classification"]])
# print("_______________________________________________")
# print(df.loc[:,"date"] )
# print("-----------------------------------------------")

# print("-----------------------------------------------")
# print(df[["date", "temperatura" ,"classification"]])
# print("_______________________________________________")
# print(df.iloc[:,1:3] )
# print("-----------------------------------------------")


# print("-----------------------------------------------")
# print(df[["date", "temperatura" ,"classification"]])
# print("_______________________________________________")
# print(df.loc[:,["date", "temperatura"]] )
# print("-----------------------------------------------")

# print("-----------------------------------------------")
# print(df[["date", "temperatura" ,"classification"]])
# print("_______________________________________________")
# print(df.loc[:,"temperatura":] )
# print("-----------------------------------------------")

# print("-----------------------------------------------")
# print(df[["date", "temperatura" ,"classification"]])
# print("_______________________________________________")
# print(df.dtypes)
# print("-----------------------------------------------")

# print("-----------------------------------------------")
# print(df[["date", "temperatura" ,"classification"]])
# print("_______________________________________________")
# df['date'] = pd.to_datetime(df['date'])
# print(df.dtypes)
# print("-----------------------------------------------")


# print("-----------------------------------------------")
# print(df[["date", "temperatura" ,"classification"]])
# print("_______________________________________________")
# df_indexed = df.set_index("date")
# print(df)
# print("-----------------------------------------------")

# print("-----------------------------------------------")
# print(df[["date", "temperatura" ,"classification"]])
# print("_______________________________________________")
# print(df[df['temperatura']>24])
# print("-----------------------------------------------")

# print("-----------------------------------------------")
# print(df[["date", "temperatura" ,"classification"]])
# print("_______________________________________________")
# print(df[df_indexed.index <= "2020-03-01"])
# print("-----------------------------------------------")

# print("-----------------------------------------------")
# print(df[["date", "temperatura" ,"classification"]])
# print("_______________________________________________")
# print(df.loc[df_indexed.index <= "2020-03-01", 'classification'])
# print("-----------------------------------------------")

# print("-----------------------------------------------")
# print(df[["date", "temperatura" ,"classification"]])
# print("_______________________________________________")
# print(df.iloc[df_indexed.index <= "2020-03-01", [-1]])
# print("-----------------------------------------------")

# print("-----------------------------------------------")
# print(df[["date", "temperatura" ,"classification"]])
# print("_______________________________________________")
# df_indexed = df_indexed.sort_values(by=['temperatura'])
# print(df_indexed)
# print("-----------------------------------------------")

# print("-----------------------------------------------")
# print(df[["date", "temperatura" ,"classification"]])
# print("_______________________________________________")
# df_indexed = df_indexed.sort_values(by=['temperatura'], na_position='first')
# print(df_indexed)
# print("-----------------------------------------------")

# print("-----------------------------------------------")
# print(df[["date", "temperatura" ,"classification"]])
# print("_______________________________________________")
# df_indexed = df_indexed.sort_index(ascending=False)
# print(df_indexed)
# print("-----------------------------------------------")

# print("-----------------------------------------------")
# import matplotlib.pyplot as plt
# plt.switch_backend('TkAgg')

# print(df[["date", "temperatura" ,"classification"]])
# print("_______________________________________________")
# df.plot(figsize=(10,5))
# plt.show()
# print("-----------------------------------------------")

# print(df[["date", "temperatura" ,"classification"]])
# print("_______________________________________________")
# df.plot(figsize=(10,5), grid=True)
# plt.show()
# print("-----------------------------------------------")


# print(df[["date", "temperatura" ,"classification"]])
# print("_______________________________________________")
# df.plot(figsize=(10,5), grid=True, style="g--")
# plt.show()
# print("-----------------------------------------------")

# print(df[["date", "temperatura" ,"classification"]])
# print("_______________________________________________")
# df.plot(figsize=(10,5), grid=True, style="g")
# plt.show()
# print("-----------------------------------------------")

# print(df[["date", "temperatura" ,"classification"]])
# print("_______________________________________________")
# df.plot(figsize=(10,5), grid=True, style="o")
# plt.show()
# print("-----------------------------------------------")

# print(df[["date", "temperatura" ,"classification"]])
# print("_______________________________________________")
# df.plot(figsize=(10,5), grid=True, style="--.")
# plt.show()
# print("-----------------------------------------------")

# print(df[["date", "temperatura" ,"classification"]])
# print("_______________________________________________")
# df.plot(figsize=(10,5), grid=True, style="-o")
# plt.show()
# print("-----------------------------------------------")


# print(df[["date", "temperatura" ,"classification"]])
# print("_______________________________________________")
# df.plot(figsize=(10,5), grid=True, style="-o", linewidth=1.5)
# plt.show()
# print("-----------------------------------------------")

# print(df[["date", "temperatura" ,"classification"]])
# print("_______________________________________________")
# df.plot(figsize=(10,5), grid=True, style="-o", linewidth=1.5, color="blue")
# plt.show()
# print("-----------------------------------------------")

# print(df[["date", "temperatura" ,"classification"]])
# print("_______________________________________________")
# df['classification'].value_counts().plot.bar(figsize=(10,5), rot=0)
# plt.show()
# print("-----------------------------------------------")

# print(df[["date", "temperatura" ,"classification"]])
# print("_______________________________________________")
# df['classification'].value_counts().plot.pie(figsize=(10,5), autopct="%1.1f%%" ,shadow=True)
# plt.show()
# print("-----------------------------------------------")

# print(df[["date", "temperatura" ,"classification"]])
# print("_______________________________________________")
# df['classification'].value_counts().plot.pie(figsize=(10,5), rot=rot90)
# plt.show()

# print("-----------------------------------------------")
# print(df[["date", "temperatura" ,"classification"]])
# print("_______________________________________________")
# df['temperatura'].plot.pie(figsize=(10,5), autopct="%1.1f%%" ,shadow=True, rot=0)
# plt.show()
# print("-----------------------------------------------")


print("-----------------------------------------------")
print(df[["date", "temperatura" ,"classification"]])
print("_______________________________________________")
df_indexed = df.set_index("date")
print(df_indexed.groupby(by="classification").mean())
print("-----------------------------------------------")

# print("-----------------------------------------------")
# print(df[["date", "temperatura" ,"classification"]])
# print("_______________________________________________")
# df_indexed = df.set_index("date")
# print(df_indexed.groupby(by="classification").max())
# print("-----------------------------------------------")


# print("-----------------------------------------------")
# print(df[["date", "temperatura" ,"classification"]])
# print("_______________________________________________")
# print(df.drop("temperatura", axis=1))
# print("-----------------------------------------------")

# print("-----------------------------------------------")
# print(df[["date", "temperatura" ,"classification"]])
# print("_______________________________________________")
# df2 = df.copy()
# df2.drop("temperatura", axis=1, inplace=True)
# print(df2.head())
# print("-----------------------------------------------")