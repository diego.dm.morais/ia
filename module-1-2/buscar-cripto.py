
# API Key e Secret da Binance
api_key = '5sbcPvrL1Aoncep4J5ZvlVc811gw6eOHyqo8muOVQxiGFpV4vmHqJJQ3WMLrrfYE'
api_secret = 'J8bAW190EjHmAdI9uEe4d0OIrjAF3VprafbOa0ciUxTXnFHf6QG5bdZYobyVOSUU'

import logging
from binance.spot import Spot as Client
from binance.lib.utils import config_logging
from binance.error import ClientError

config_logging(logging, logging.DEBUG)
logger = logging.getLogger(__name__)

client = Client(api_key, api_secret)

try:
    response = client.get_rate_history(
        "product_001", current=1, size=100, recvWindow=5000
    )
    logger.info(response)
except ClientError as error:
    logger.error(
        "Found error. status: {}, error code: {}, error message: {}".format(
            error.status_code, error.error_code, error.error_message
        )
    )