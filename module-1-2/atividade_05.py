def obter_numero_impares(numeros: list[int]) -> list[int]:
    return sorted([item for item in numeros if item % 2 == 1])


def maior_impar(numeros: list[int]) -> int:
    return obter_numero_impares(numeros)[-1]


def menor_impar(numeros: list[int]):
    return obter_numero_impares(numeros)[0]


def menor_maior_impar(numeros: list[int]) -> tuple:
    return menor_impar(numeros), maior_impar(numeros)


lista = [1, 2, 3, 4, 5, 95, 6, 7, 8, 9, 10, 11, 12, 13, 15, 16]

res_1 = maior_impar(lista)
res_2 = menor_impar(lista)
res_3 = menor_maior_impar(lista)

print(f"Tipo {type(res_1)}, resultado: {res_1}")
print(f"Tipo {type(res_2)}, resultado: {res_2}")
print(f"Tipo {type(res_3)}, resultado: {res_3}")
