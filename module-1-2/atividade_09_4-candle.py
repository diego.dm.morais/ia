import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter
import requests
from datetime import datetime

url = 'https://api.kraken.com/0/public/OHLC'
params = {
    'pair': 'BTCUSD',
    'interval': '15',
    'since': '1711166461'
}

response = requests.get(url, params=params)

data = {}

if response.status_code == 200:
    data = response.json()
    if 'result' in data and 'XXBTZUSD' in data['result']:
        result = data['result']['XXBTZUSD']
        formatted_data = {
            'Date': [],
            'Open': [],
            'High': [],
            'Low': [],
            'Close': []
        }
        for item in result:
            formatted_data['Date'].append(
                datetime.fromtimestamp(item[0]).strftime('%Y-%m-%d %H:%M'))
            formatted_data['Open'].append(float(item[1]))
            formatted_data['High'].append(float(item[2]))
            formatted_data['Low'].append(float(item[3]))
            formatted_data['Close'].append(float(item[4]))
        df = pd.DataFrame(formatted_data)
    else:
        print("Não foi possível encontrar os dados necessários na resposta.")
else:
    print(
        f"Erro ao fazer a solicitação. Código de status: {response.status_code}")

# Método para identificar os candles Doji
def is_doji(open_price, close_price, high_price, low_price):
    # Calcula o tamanho do corpo do candle
    body_size = abs(close_price - open_price)
    # Calcula as sombras superior e inferior
    upper_shadow = high_price - max(open_price, close_price)
    lower_shadow = min(open_price, close_price) - low_price
    # Define os critérios para identificar o Doji Comum
    return body_size < 0.03 * (high_price - low_price) and upper_shadow > 2 * body_size and lower_shadow > 2 * body_size

def is_long_legged_doji(open_price, close_price, high_price, low_price):
    # Calcula o tamanho do corpo do candle
    body_size = abs(close_price - open_price)
    # Calcula as sombras superior e inferior
    upper_shadow = high_price - max(open_price, close_price)
    lower_shadow = min(open_price, close_price) - low_price
    # Define os critérios para identificar o Long-legged Doji
    return body_size < 0.01 * (high_price - low_price) and upper_shadow > 2 * body_size and lower_shadow > 2 * body_size

def is_gravestone_doji(open_price, close_price, high_price, low_price):
    # Calcula o tamanho do corpo do candle
    body_size = abs(close_price - open_price)
    # Verifica se o preço máximo é significativamente maior que o preço de abertura e fechamento
    return body_size < 0.03 * (high_price - low_price) and high_price > max(open_price, close_price) * 1.005

def is_dragonfly_doji(open_price, close_price, high_price, low_price):
    # Calcula o tamanho do corpo do candle
    body_size = abs(close_price - open_price)
    # Verifica se o preço mínimo é significativamente menor que o preço de abertura e fechamento
    return body_size < 0.03 * (high_price - low_price) and low_price < min(open_price, close_price) * 0.995

def is_red_marubozu(open_price, close_price, high_price, low_price):
    # Verifica se o candle é vermelho e se não possui sombras
    return close_price == low_price and open_price > close_price

def is_green_marubozu(open_price, close_price, high_price, low_price):
    # Verifica se o candle é verde e se não possui sombras
    return close_price == high_price and open_price < close_price



# Adiciona uma coluna indicando se o candle é Doji ou não
df['IsDojiCommon'] = df.apply(lambda row: is_doji(row['Open'], row['Close'], row['High'], row['Low']), axis=1)
df['IsLongLeggedDoji'] = df.apply(lambda row: is_long_legged_doji(row['Open'], row['Close'], row['High'], row['Low']), axis=1)
df['IsGravestoneDoji'] = df.apply(lambda row: is_gravestone_doji(row['Open'], row['Close'], row['High'], row['Low']), axis=1)
df['IsDragonflyDoji'] = df.apply(lambda row: is_dragonfly_doji(row['Open'], row['Close'], row['High'], row['Low']), axis=1)
df['IsRedMarubozu'] = df.apply(lambda row: is_red_marubozu(row['Open'], row['Close'], row['High'], row['Low']), axis=1)
df['IsGreenMarubozu'] = df.apply(lambda row: is_green_marubozu(row['Open'], row['Close'], row['High'], row['Low']), axis=1)


# Filtra os candles Doji
doji_common_df = df[df['IsDojiCommon'] == True]
long_legged_doji_df = df[df['IsLongLeggedDoji'] == True]
gravestone_doji_df = df[df['IsGravestoneDoji'] == True]
dragonfly_doji_df = df[df['IsDragonflyDoji'] == True]
red_marubozu_df = df[df['IsRedMarubozu'] == True]
green_marubozu_df = df[df['IsGreenMarubozu'] == True]

# Create the candlestick chart
plt.figure(figsize=(18, 12))  # Adjust figure size as needed

# Define candle colors based on price movement
def candle_color(open_price, close_price):
    if close_price > open_price:
        return 'green'
    elif close_price < open_price:
        return 'red'
    else:
        return 'black'

df['Color'] = df.apply(lambda row: candle_color(
    row['Open'], row['Close']), axis=1)

# Candlestick bodies (adjusting width)
plt.bar(df['Date'], df['Close'] - df['Open'],
        bottom=df['Open'], color=df['Color'], width=1, zorder=2)

orange = 'orange'
purple = 'purple'
pink = 'pink'
plt.bar(df['Date'], df['Close'] - df['Open'],
        bottom=df['Open'], color=np.where(df['IsDragonflyDoji'], orange, np.where(df['IsGravestoneDoji'], 'red', df['Color'])), width=1, zorder=2)
# Candlestick bodies (adjusting width)
plt.bar(df['Date'], df['Close'] - df['Open'],
        bottom=df['Open'], color=np.where(df['IsRedMarubozu'], purple, np.where(df['IsGreenMarubozu'], pink, df['Color'])), width=1, zorder=2)


# Upper and lower wicks (lines with specified width)
plt.vlines(x=df['Date'], ymin=df['Low'], ymax=df['Open'],
           color='black', linewidth=0.8, zorder=1)  # Lower wick
plt.vlines(x=df['Date'], ymin=df['Close'], ymax=df['High'],
           color='black', linewidth=0.8, zorder=1)  # Upper wick

# Plotting Doji candles separately
plt.scatter(doji_common_df['Date'], doji_common_df['Close'],
            color='blue', marker='o', label='doji_common', zorder=3)
plt.scatter(long_legged_doji_df['Date'], long_legged_doji_df['Close'],
            color='lightblue', marker='o', label='long_legged_doji', zorder=3)
plt.scatter(gravestone_doji_df['Date'], gravestone_doji_df['Close'],
            color='red', marker='o', label='gravestone_doji_df', zorder=3)
plt.scatter(dragonfly_doji_df['Date'], dragonfly_doji_df['Close'],
            color=orange, marker='o', label='dragonfly_doji_df', zorder=3)
plt.scatter(red_marubozu_df['Date'], red_marubozu_df['Close'],
            color=purple, marker='o', label='red_marubozu_df', zorder=3)
plt.scatter(green_marubozu_df['Date'], green_marubozu_df['Close'],
            color=pink, marker='o', label='green_marubozu_df', zorder=3)

# Customize axes labels and title
plt.xlabel('Date')
plt.ylabel('Price')
plt.title('Candlestick Chart with Doji Candles')

# Rotate x-axis labels for better readability with many dates
plt.xticks(df['Date'][::10], rotation=45)

# Annotate data points with date and value
for i in range(len(df)):
    plt.annotate(f'{df["High"][i]:.2f}\n{df["Close"][i]:.2f}\n{df["Open"][i]:.2f}\n{df["Low"][i]:.2f}', (df["Date"][i], df["Close"]
                 [i]), textcoords="offset points", xytext=(0, 10), ha='center')

plt.tight_layout()
plt.legend()
plt.show()
