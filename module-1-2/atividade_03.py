PI = 3.14159265359


def area(r, pi):
    return round(pi * (r**2), 2)


area_x = area(8, PI)

print(area_x)
