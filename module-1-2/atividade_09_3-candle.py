import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter
import requests
from datetime import datetime

url = 'https://api.kraken.com/0/public/OHLC'
params = {
    'pair': 'BTCUSD',
    'interval': '60',
    'since': '1711166461'
}

response = requests.get(url, params=params)

data = {}

if response.status_code == 200:
    data = response.json()
    print("Dados brutos da API Kraken:")
    if 'result' in data and 'XXBTZUSD' in data['result']:
        result = data['result']['XXBTZUSD']
        formatted_data = {
            'Date': [],
            'Open': [],
            'High': [],
            'Low': [],
            'Close': []
        }
        for item in result:
            formatted_data['Date'].append(
                datetime.fromtimestamp(item[0]).strftime('%Y-%m-%d %H:%M'))
            formatted_data['Open'].append(float(item[1]))
            formatted_data['High'].append(float(item[2]))
            formatted_data['Low'].append(float(item[3]))
            formatted_data['Close'].append(float(item[4]))
        df = pd.DataFrame(formatted_data)
    else:
        print("Não foi possível encontrar os dados necessários na resposta.")
else:
    print(
        f"Erro ao fazer a solicitação. Código de status: {response.status_code}")

# Create the candlestick chart
plt.figure(figsize=(18, 12))  # Adjust figure size as needed

# Define candle colors based on price movement


def candle_color(open_price, close_price):
    if close_price > open_price:
        return 'green'
    elif close_price < open_price:
        return 'red'
    else:
        return 'black'


df['Color'] = df.apply(lambda row: candle_color(
    row['Open'], row['Close']), axis=1)

# Candlestick bodies (adjusting width)
plt.bar(df['Date'], df['Close'] - df['Open'],
        bottom=df['Open'], color=df['Color'], width=1, zorder=2)

# Upper and lower wicks (lines with specified width)
plt.vlines(x=df['Date'], ymin=df['Low'], ymax=df['Open'],
           color='black', linewidth=0.8, zorder=1)  # Lower wick
plt.vlines(x=df['Date'], ymin=df['Close'], ymax=df['High'],
           color='black', linewidth=0.8, zorder=1)  # Upper wick

# Customize axes labels and title
plt.xlabel('Date')
plt.ylabel('Price')
plt.title('Candlestick Chart')

# Rotate x-axis labels for better readability with many dates
plt.xticks(df['Date'][::10], rotation=45)

# Annotate data points with date and value
for i in range(len(df)):
    plt.annotate(f'{df["High"][i]:.2f}\n{df["Close"][i]:.2f}\n{df["Open"][i]:.2f}\n{df["Low"][i]:.2f}', (df["Date"][i], df["Close"]
                 [i]), textcoords="offset points", xytext=(0, 10), ha='center')

plt.tight_layout()
plt.show()
