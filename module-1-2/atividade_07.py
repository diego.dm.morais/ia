import numpy as np

linha_0 = [1,2,3]
linha_1 = [4,5,6]
linha_2 = [7,8,9]

a = np.array([linha_0,linha_1, linha_2])
print(a)
print(a.shape)

print(np.eye(3))
print(np.random.random(size=(3,3)))

print(np.linspace(start=0, stop=1, num=2, dtype=float))

print(a[1,-1])

b= np.random.random(size=(2,2,2))
print(b)

b[1,1,1] = 5
print(b[1,0,1])

def change(ab):
    ab[0] = -100


x = np.array([1,2,3])
print(x)

y = x[:2].copy()
change(y)

print(y)
print(x)

x = 2 * np.ones(shape=(2,2))
y = np.ones(shape=(2,2))
y[0,0]=2
print("--------------------------------------------")
print(x)
print("--------------------------------------------")
print(y)
print("-----------------a = x * y---------------------------")
a = x * y
print(a)
print("-------------a = x + 2-------------------------------")
a = x + 2
print(a)
print("-------------b = x @ y-------------------------------")
# b=  x @ y
# print(b)
print("---------------np.dot(x, y)-----------------------------")
print(x)
print("*******************")
print(y)
print("*******************")
print(np.dot(x, y))
print("*******************")

A = np.array([[1, 2], [3, 4]])
B = np.array([[5, 6], [7, 8]])

# 1 * 5 + 2 * 7
# 1 * 6 + 2 * 8
# 3 * 5 + 4 * 7
# 3 * 6 + 4 * 8


print(np.dot(A, B))
print(A @ B)
print("--------------------------------------------")

# Ax = c
# x = C/A

print("--------------------------------------------")
A = np.array([[1,2],[3,-2]])
print(f"A: {A}")
print("--------------------------------------------")

C = np.array([[7],[-11]])
print(f"C: {C}")
print("--------------------------------------------")



a_temp = np.linalg.inv(A)
print(f"a_temp: {a_temp}")
print("--------------------------------------------")

x = np.dot(a_temp, C)
print(f"{x.ravel()}")
print("--------------------------------------------")
x = np.array([[1,2],[2,3],[3,4]])
print(x == x)
print("--------------------------------------------")
print(x[x > 2])
