# Comandos
## Comando para criar o projeto
```shell
python3 -m venv ./.venv
source .venv/bin/activate
pip install -r requirements.txt
```

## comando install
comando opcional

```shell
pip freeze > requirements.txt
```


```shell
pip install -r requirements.txt
```

## comando para criar serviço Django
```shell
django-admin startproject user_service
```

## comando para executar o o serviço
```shell
python manage.py runserver 0.0.0.0:8000
```

## comando para criar um app

```shell
cd ./user_service/
python manage.py startapp app1
```

## comando para criar os arquivos migraões do banco de dados com base nos models

```shell
python manage.py makemigrations
echo "criar tabelas no sqlite"
python manage.py migrate
```

# comando para criar um supser usuário do sistema

```shell
python manage.py createsuperuser
```