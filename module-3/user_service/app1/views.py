from django.shortcuts import render
from django.http import HttpRequest
from .forms import UserForm


def index(request: HttpRequest):
    return render(request, 'user/index.html')


def user_create(request: HttpRequest):
    if request.method == 'GET':
        context = {
            'form': UserForm(),
        }

    return render(request, "user/create.html", context=context)
