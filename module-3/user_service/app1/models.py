from os import name
from django.db import models


class User(models.Model):
    nome = models.CharField('nome', max_length=30)
    telefone = models.BigIntegerField('telefone')
    email = models.CharField('email', max_length=30)

    def __str__(self) -> str:
        return f"nome: {self.nome}, telefone: {self.telefone}, email:{self.email}"
