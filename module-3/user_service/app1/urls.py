from django.contrib import admin
from django.urls import path

from .views import index
from .views import user_create

urlpatterns = [
    path('', index, name="index"),
    path('create/user', user_create, name="user_create"),
]
