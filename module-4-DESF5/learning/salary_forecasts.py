from datetime import date
import pandas as pd
from sklearn.preprocessing import LabelEncoder, StandardScaler
from sklearn.neural_network import MLPRegressor
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.metrics import mean_squared_error
import pickle


REQUIRED_COLUMNS = ["years_of_experience", "classification", "salary"]


class SalaryForecastsMlp:
    _instance = None

    def __new__(cls):
        if cls._instance is None:
            cls._instance = super().__new__(cls)
            # cls._instance._X = None
            # cls._instance._y = None

        return cls._instance

    def create_model(self, file_path: str):
        data = pd.read_csv(file_path)
        data.dropna(inplace=True)

        if self.validate_csv(data):

            # Separar os recursos (features) e rótulos (labels)
            X = data.iloc[:, 0:-1].values
            y = data.iloc[:, -1].values

            label_enconder = LabelEncoder()
            X[:, 1] = label_enconder.fit_transform(X[:, 1])

            # Dividir os dados em conjuntos de treinamento e teste
            X_train, X_test, y_train, y_test = train_test_split(
                X, y, test_size=0.3, random_state=0
            )

            # Padronizar os dados
            scaler = StandardScaler()
            X_train_scaled = scaler.fit_transform(X_train)
            X_test_scaled = scaler.transform(X_test)

            # Definir os parâmetros para busca em grade
            param_grid = {
                'hidden_layer_sizes': [(100,), (50,), (100, 50), ( 50, 100)],
                'activation': ['identity', 'logistic', 'tanh', 'relu'],
                'solver': ['lbfgs', 'sgd', 'adam'],
                'max_iter': [200, 500, 3000],
                'learning_rate_init': [0.0001, 0.001, 0.01, 0.1],
                'random_state': [0]
            }

            # Inicializar o modelo MLPRegressor
            mlp_regressor = MLPRegressor()

            # Criar um objeto GridSearchCV
            grid_search = GridSearchCV(estimator=mlp_regressor, param_grid=param_grid, cv=5)

            # Ajustar o modelo aos dados
            grid_search.fit(X_train_scaled, y_train)

            # Obter o melhor modelo
            best_model = grid_search.best_estimator_

            # Avaliar o desempenho do modelo
            y_pred_train = best_model.predict(X_train_scaled)
            y_pred_test = best_model.predict(X_test_scaled)

            mse_train = mean_squared_error(y_train, y_pred_train)
            mse_test = mean_squared_error(y_test, y_pred_test)

            print("Melhores parâmetros encontrados:", grid_search.best_params_)
            print("Erro médio quadrático (MSE) no conjunto de treinamento:", mse_train)
            print("Erro médio quadrático (MSE) no conjunto de teste:", mse_test)

            filename = f"upload/training/model_mlpr_{date.today().isoformat()}"
            with open(filename, "wb") as file:
                pickle.dump(best_model, file)

            return filename

    def validate_csv(self, data: pd.DataFrame):
        try:
            return all(col in data.columns for col in REQUIRED_COLUMNS)
        except Exception as e:
            raise e


if __name__ == "__main__":
    salary_forecasts_mlp = SalaryForecastsMlp()
    salary_forecasts_mlp.create_model("upload/salaries_20240502.csv")
