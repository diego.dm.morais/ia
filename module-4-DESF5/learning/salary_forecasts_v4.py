import pandas as pd
from sklearn.preprocessing import LabelEncoder, StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPRegressor
from sklearn.metrics import mean_squared_error

def load_data(file_path):
    """Carrega os dados de um arquivo CSV."""
    return pd.read_csv(file_path)

def encode_classification(df, column_name):
    """Codifica a variável categórica usando LabelEncoder."""
    label_encoder = LabelEncoder()
    df[column_name] = label_encoder.fit_transform(df[column_name])
    return df, label_encoder

def train_model(X_train, y_train):
    """Inicializa e treina o modelo de regressão MLP."""
    model = MLPRegressor(hidden_layer_sizes=(50,), max_iter=200, random_state=0)
    model.fit(X_train, y_train)
    return model

def evaluate_model(model, X_test, y_test):
    """Avalia o modelo usando o conjunto de teste."""
    y_pred = model.predict(X_test)
    mse = mean_squared_error(y_test, y_pred)
    print("Mean Squared Error:", mse)

def predict_new_data(model, new_data, label_encoder, scaler):
    """Faz a previsão para novos dados usando o modelo treinado."""
    new_data_df = pd.DataFrame(new_data)
    new_data_df["classification"] = label_encoder.transform(new_data_df["classification"])
    new_data_scaled = scaler.transform(new_data_df)
    predicted_salary = model.predict(new_data_scaled)
    print("Previsão de salário para os novos dados:", predicted_salary)

def main():
    # Carrega os dados
    df = load_data("upload/salaries_20240502.csv")

    # Codifica a variável categórica "classification"
    df, label_encoder = encode_classification(df, "classification")

    # Dividindo os dados em conjunto de treinamento e teste
    X = df.drop("salary", axis=1)
    y = df["salary"]
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.30, random_state=0)

    # Normalizando os dados
    scaler = StandardScaler()
    X_train_scaled = scaler.fit_transform(X_train)
    X_test_scaled = scaler.transform(X_test)

    # Treinando o modelo
    model = train_model(X_train_scaled, y_train)

    # Avaliando o modelo
    evaluate_model(model, X_test_scaled, y_test)

    # Novos dados para prever
    new_data = {
        "years_of_experience": [8],
        "classification": ["senior_specialist"],
    }

    # Fazendo a previsão
    predict_new_data(model, new_data, label_encoder, scaler)

if __name__ == "__main__":
    main()
