# Importando as bibliotecas necessárias
import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error

# Carregando os dados
df = pd.read_csv("upload/salaries_20240502.csv")

# Criando um objeto LabelEncoder
label_encoder = LabelEncoder()

# Codificando a variável categórica "classification"
df["classification"] = label_encoder.fit_transform(df["classification"])

# Dividindo os dados em conjunto de treinamento e teste
X = df.drop("salary", axis=1)
y = df["salary"]
X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.35, random_state=42
)

# Inicializando e treinando o modelo de regressão linear
model = LinearRegression()
model.fit(X_train, y_train)

# Fazendo previsões no conjunto de teste
y_pred = model.predict(X_test)

# Avaliando o modelo
mse = mean_squared_error(y_test, y_pred)
print("Mean Squared Error:", mse)

# Coeficientes do modelo
print("Coeficientes do modelo:", model.coef_)
print("Intercepto do modelo:", model.intercept_)

# Novos dados para prever
new_data = {
    "years_of_experience": [1.1],
    "classification": ["intern"],
}

# Codificando a variável categórica "classification" nos novos dados
new_data["classification"] = label_encoder.transform(new_data["classification"])

# Criando um DataFrame com os novos dados
new_df = pd.DataFrame(new_data)

# Fazendo a previsão
predicted_salary = model.predict(new_df)

# Imprimindo a previsão de salário
print("Previsão de salário para os novos dados:", predicted_salary)
