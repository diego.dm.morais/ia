import pandas as pd
from sklearn.neural_network import MLPRegressor
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error

# Carregar os dados do CSV
data = pd.read_csv("upload/salaries_20240502.csv")

# Separar as características (features) e o alvo (target)
X = data[["years_of_experience", "classification"]]
y = data["salary"]

# Converter variáveis categóricas em variáveis dummy
X = pd.get_dummies(X)

# Dividir os dados em conjuntos de treinamento e teste
X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.2, random_state=42
)

# Normalizar as características
scaler = StandardScaler()
X_train_scaled = scaler.fit_transform(X_train)
X_test_scaled = scaler.transform(X_test)

# Inicializar e treinar o modelo MLPRegressor
mlp_regressor = MLPRegressor(
    hidden_layer_sizes=(100, 50),
    max_iter=2000,
    activation="relu",
    solver="lbfgs",
    alpha=0.0001,
)
mlp_regressor.fit(X_train_scaled, y_train)

# Avaliar o modelo
y_pred = mlp_regressor.predict(X_test_scaled)
mse = mean_squared_error(y_test, y_pred)
print("Mean Squared Error:", mse)

# Fazer previsões para novos profissionais
# Suponha que você tenha novas_caracteristicas em um DataFrame com as mesmas colunas que X

# Criar um DataFrame com as novas características
novas_caracteristicas = pd.DataFrame(
    {"years_of_experience": [1.1], "classification": ["intern"]}
)

# Converter a coluna de classificação em variáveis dummy e garantir que as colunas correspondam às colunas usadas para treinar o modelo
novas_caracteristicas = pd.get_dummies(novas_caracteristicas).reindex(
    columns=X.columns, fill_value=0
)

# Normalizar as características e fazer a previsão usando o modelo treinado
previsoes = mlp_regressor.predict(scaler.transform(novas_caracteristicas))
print("Previsões de salário:", previsoes)
