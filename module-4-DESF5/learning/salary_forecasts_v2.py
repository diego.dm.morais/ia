from datetime import datetime
import warnings
import pandas as pd
from sklearn.preprocessing import LabelEncoder, StandardScaler
from sklearn.neural_network import MLPRegressor
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.metrics import mean_squared_error
import pickle
import numpy as np

REQUIRED_COLUMNS = ["years_of_experience", "classification", "salary"]


class SalaryForecastsMlp:
    def __init__(self):
        self._instance = None
        self._punishment_factor = 0.1  # Fator de punição inicial

    def create_model(self, file_path: str):
        data = pd.read_csv(file_path)
        data.dropna(inplace=True)

        if self.validate_csv(data):

            # Separar os recursos (features) e rótulos (labels)
            X = data.iloc[:, 0:-1].values
            y = data.iloc[:, -1].values

            label_encoder = LabelEncoder()
            X[:, 1] = label_encoder.fit_transform(X[:, 1])

            # Dividir os dados em conjuntos de treinamento e teste
            X_train, X_test, y_train, y_test = train_test_split(
                X, y, test_size=0.3, random_state=0
            )

            # Padronizar os dados
            scaler = StandardScaler()
            X_train_scaled = scaler.fit_transform(X_train)
            X_test_scaled = scaler.transform(X_test)

            param_grid = {
                "hidden_layer_sizes": [(100,), (50,)],
                "activation": ["identity", "logistic", "tanh", "relu"],
                "solver": ["lbfgs", "sgd", "adam"],
                "max_iter": [200, 1000],
                "learning_rate_init": [0.001, 0.01],
                "random_state": [0, 42, 50],
            }

            # Inicializar o modelo MLPRegressor
            mlp_regressor = MLPRegressor()

            # Criar um objeto GridSearchCV
            grid_search = GridSearchCV(
                estimator=mlp_regressor, param_grid=param_grid, cv=5
            )

            # Ajustar o modelo aos dados
            grid_search.fit(X_train_scaled, y_train)

            # Obter o melhor modelo
            best_model = grid_search.best_estimator_

            # Avaliar o desempenho do modelo
            y_pred_train = best_model.predict(X_train_scaled)
            y_pred_test = best_model.predict(X_test_scaled)

            mse_train = mean_squared_error(y_train, y_pred_train)
            mse_test = mean_squared_error(y_test, y_pred_test)

            print("Melhores parâmetros encontrados:", grid_search.best_params_)
            print("Erro médio quadrático (MSE) no conjunto de treinamento:", mse_train)
            print("Erro médio quadrático (MSE) no conjunto de teste:", mse_test)

            filename = f"upload/training/model_mlpr_{datetime.today().strftime('%Y%m%d%H%M%S')}.pkl"
            with open(filename, "wb") as file:
                pickle.dump(best_model, file)

            self._instance = best_model

            return filename

    def validate_csv(self, data: pd.DataFrame):
        try:
            return all(col in data.columns for col in REQUIRED_COLUMNS)
        except Exception as e:
            raise e

    def reinforce_learning(self, predicted_salary, actual_salary):
        # Atualizar o fator de punição com base na diferença entre a previsão e o salário real
        error = abs(predicted_salary - actual_salary)
        self._punishment_factor += error * 0.01  # Ajuste do fator de punição

    def predict_salary(self, new_data_scaled):
        predicted_salary = self._instance.predict(new_data_scaled)
        return predicted_salary

    def update_punishment_factor(self, factor):
        self._punishment_factor = factor

    def get_punishment_factor(self):
        return self._punishment_factor


if __name__ == "__main__":
    salary_forecasts_mlp = SalaryForecastsMlp()

    # Treinar o modelo e salvar
    model_filename = salary_forecasts_mlp.create_model("upload/salaries_20240502.csv")

    # Carregar o modelo treinado
    with open(model_filename, "rb") as file:
        trained_model = pickle.load(file)
        salary_forecasts_mlp._instance = trained_model

    new_data = pd.DataFrame(
        {
            "years_of_experience": [1.8],
            "classification": ["intern"],
        }
    )

    label_encoder = LabelEncoder()
    new_data["classification"] = label_encoder.fit_transform(new_data["classification"])

    scaler = StandardScaler()
    new_data_scaled = scaler.fit_transform(new_data)

    predicted_salary = salary_forecasts_mlp.predict_salary(new_data_scaled)
    print("Previsão de salário:", predicted_salary[0])

    actual_salary = 2500  # Assume-se um salário real para fins de exemplo

    # Ajuste do fator de punição com base no erro entre a previsão e o salário real
    salary_forecasts_mlp.reinforce_learning(predicted_salary[0], actual_salary)

    # Obtendo o fator de punição atualizado
    punishment_factor = salary_forecasts_mlp.get_punishment_factor()
    print("Fator de punição atualizado:", punishment_factor)
