import pickle
import pandas as pd
from sklearn.neural_network import MLPRegressor
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
from datetime import datetime
import copy

# Carregar os dados do CSV
data = pd.read_csv("upload/salaries_20240502.csv")

# Separar as características (features) e o alvo (target)
X = data[["years_of_experience", "classification"]]
y = data["salary"]

# Converter variáveis categóricas em variáveis dummy
X = pd.get_dummies(X)

# Dividir os dados em conjuntos de treinamento e teste
X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.1, random_state=42
)

# Normalizar as características
scaler = StandardScaler()
X_train_scaled = scaler.fit_transform(X_train)
X_test_scaled = scaler.transform(X_test)

filename = "upload/training/model_mlpr_20240503091355.pkl"
try:
    with open(filename, "rb") as file:
        mlp_regressor = pickle.load(file)
except FileNotFoundError:
    mlp_regressor = None


if not mlp_regressor:
    mlp_regressor = MLPRegressor(
        hidden_layer_sizes=(100, 50),
        max_iter=2000,
        activation="relu",
        solver="lbfgs",
        alpha=0.0001,
    )

    mlp_regressor.fit(X_train_scaled, y_train)

    # Avaliar o modelo
    y_pred_train = mlp_regressor.predict(X_train_scaled)
    y_pred_test = mlp_regressor.predict(X_test_scaled)

    mse_train = mean_squared_error(y_train, y_pred_train)
    mse_test = mean_squared_error(y_test, y_pred_test)

    print("Erro médio quadrático (MSE) no conjunto de treinamento:", mse_train)
    print("Erro médio quadrático (MSE) no conjunto de teste:", mse_test)

    filename = (
        f"upload/training/model_mlpr_{datetime.today().strftime('%Y%m%d%H%M%S')}.pkl"
    )
    with open(filename, "wb") as file:
        pickle.dump(mlp_regressor, file)

# Fazer previsões para novos profissionais
# Suponha que você tenha novas_caracteristicas em um DataFrame com as mesmas colunas que X

dados = [
    {"years_of_experience": 0, "classification": "aprendiz"},
]

dados_copy = copy.deepcopy(dados)

years_of_experience = [item["years_of_experience"] for item in dados_copy]
classification = [item["classification"] for item in dados_copy]

data_frame = {
    "years_of_experience": years_of_experience,
    "classification": classification,
}


# Criar um DataFrame com as novas características
novas_caracteristicas = pd.DataFrame(data_frame)

# Converter a coluna de classificação em variáveis dummy e garantir que as colunas correspondam às colunas usadas para treinar o modelo
novas_caracteristicas = pd.get_dummies(novas_caracteristicas).reindex(
    columns=X.columns, fill_value=0
)

# Normalizar as características e fazer a previsão usando o modelo treinado
previsoes = mlp_regressor.predict(scaler.transform(novas_caracteristicas))


print("years of experience:", years_of_experience)
print("classification:", classification)
print("Previsões de salário:", previsoes.tolist())


# data = []
# for i in range(len(years_of_experience)):
#   # Create a dictionary for each data point
#   employee_data = {
#       "years_of_experience": years_of_experience[i],
#       "classification": classification[i],
#       "salary": previsoes[i]
#   }
#   # Append the dictionary to the data list
#   data.append(employee_data)

# print(data)

# fieldnames = ["years_of_experience", "classification", "salary"]

# # Create a DataFrame from the data
# df = pd.read_csv('upload/salaries_20240502.csv')

# # Convert data list to DataFrame
# new_df = pd.DataFrame(data)

# # Concatenate existing DataFrame with new DataFrame
# df = pd.concat([df, new_df], ignore_index=True)

# # Write the DataFrame to a CSV file
# df.to_csv('upload/salaries_20240502.csv', index=False)

# print("CSV file created successfully!")
