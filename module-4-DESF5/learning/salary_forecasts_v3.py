# Importando as bibliotecas necessárias
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error

# Carregando os dados
data = {
    'years_of_experience': [1.1, 2.2, 3.3, 4.4, 5.5],
    'classification': ['intern', 'junior', 'senior', 'senior', 'manager'],
    'projects_completed': [2, 3, 4, 5, 6],  # Nova informação
    'salary': [3000.0, 4000.0, 5000.0, 6000.0, 8000.0]
}


df = pd.DataFrame(data)

# Convertendo a variável categórica 'classification' em variáveis dummy
df = pd.get_dummies(df, columns=['classification'], drop_first=True)

# Dividindo os dados em conjunto de treinamento e teste
X = df.drop('salary', axis=1)
y = df['salary']
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Inicializando e treinando o modelo de regressão linear
model = LinearRegression()
model.fit(X_train, y_train)

# Fazendo previsões no conjunto de teste
y_pred = model.predict(X_test)

# Avaliando o modelo
mse = mean_squared_error(y_test, y_pred)
print("Mean Squared Error:", mse)

# Coeficientes do modelo
print("Coeficientes do modelo:", model.coef_)
print("Intercepto do modelo:", model.intercept_)
